package com.example.terminator.ring14.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.example.terminator.ring14.R;
import com.example.terminator.ring14.helpers.FirebaseConnection;
import com.example.terminator.ring14.utils.StringGenerator;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public abstract class BaseActivity extends BaseClientActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    private FirebaseAuth mAuth;

    private FirebaseAuth.AuthStateListener mAuthListener;

    private FirebaseConnection mFirebaseConnection;

    private boolean mIsAuthenticated = false;

    private boolean mIsConnected = false;

    private String mCurrentDistanceUnit;

    private final BroadcastReceiver mConnectivityBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleConnectionChange();
        }
    };

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        firebaseSetup();
        mFirebaseConnection = FirebaseConnection.getInstance();
        mFirebaseConnection.minimizeConnections(true);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mCurrentDistanceUnit = sharedPreferences.getString(getString(R.string.key_unit_pref),
                StringGenerator.getDefaultUnitSystemValue(this));
    }

    protected abstract void onFirebaseAuthenticated();

    protected abstract void onFirebaseAuthenticationFailure();

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

        mIsConnected = hasInternetConnection();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mConnectivityBroadcastReceiver, intentFilter);

        if (isUnitSystemChanged()) {
            /*
             * Call recreate() at the end of onStart() so that onStop() always
             * finds the activity in the same state.
             */
            recreate();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        unregisterReceiver(mConnectivityBroadcastReceiver);
    }

    private boolean hasInternetConnection() {
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void handleConnectionChange() {
        boolean isConnected = hasInternetConnection();
        if (isConnected == mIsConnected) {
            Log.d(TAG, "No connectivity change");
        } else {
            if (isConnected) {
                Log.d(TAG, "Connection available");
                if (!mIsAuthenticated) {
                    Log.d(TAG, "Re-authenticating");
                    beginUserInitiatedSignIn();
                }
            } else {
                Log.d(TAG, "No Connection available");
            }
        }
        mIsConnected = isConnected;
    }

    public FirebaseConnection getFirebaseConnection() {
        return mFirebaseConnection;
    }

    private void firebaseSetup() {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged:signed_in");
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {
        OptionalPendingResult<GoogleSignInResult> pendingResult =
                Auth.GoogleSignInApi.silentSignIn(getApiClient());

        if (pendingResult.isDone()) {
            GoogleSignInAccount acct = pendingResult.get().getSignInAccount();
            firebaseAuthWithGoogle(acct);
        } else {
            pendingResult.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult result) {
                    if (result.isSuccess()) {
                        GoogleSignInAccount acct = result.getSignInAccount();
                        firebaseAuthWithGoogle(acct);
                    }
                }
            });
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mIsAuthenticated = true;
                            Log.d(TAG, "signInWithCredential: authenticated successfully");
                            onFirebaseAuthenticated();
                        } else {
                            mIsAuthenticated = false;
                            Log.e(TAG, "signInWithCredential: authentication failure");
                            if (hasInternetConnection()) {
                                Toast.makeText(BaseActivity.this, R.string.error_authentication,
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(BaseActivity.this,
                                        R.string.error_authentication_connection,
                                        Toast.LENGTH_SHORT).show();
                            }
                            onFirebaseAuthenticationFailure();
                        }
                    }
                });
    }

    public void signOut() {
        if (mIsAuthenticated) {
            FirebaseAuth.getInstance().signOut();
        }
        if (getApiClient().isConnected()) {
            Auth.GoogleSignInApi.signOut(getApiClient()).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            startActivity(new Intent(BaseActivity.this, LoginActivity.class));
                            finish();
                        }
                    });
        } else {
            startActivity(new Intent(BaseActivity.this, LoginActivity.class));
            finish();
        }
    }

    public FirebaseUser getCurrentUser() {
        return mAuth.getCurrentUser();
    }

    private boolean isUnitSystemChanged() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String unit = sharedPreferences.getString(getString(R.string.key_unit_pref),
                StringGenerator.getDefaultUnitSystemValue(this));
        return !unit.equals(mCurrentDistanceUnit);
    }

    public boolean isAuthenticated() {
        return mIsAuthenticated;
    }
}
