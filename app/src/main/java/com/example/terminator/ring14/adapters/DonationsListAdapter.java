package com.example.terminator.ring14.adapters;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.terminator.ring14.R;
import com.example.terminator.ring14.activities.DonationDetailActivity;
import com.example.terminator.ring14.model.Donation;
import com.example.terminator.ring14.utils.DonationUtils;
import com.example.terminator.ring14.utils.SocialShareUtils;
import com.example.terminator.ring14.utils.StringGenerator;

import java.io.IOException;
import java.util.List;

public class DonationsListAdapter extends RecyclerView.Adapter<DonationsListAdapter.ViewHolder> {

    private List<Donation> mDonations;

    private Context mContext;

    private int mParentWidth;

    private boolean mAnimateCards = true;
    private int mAnimationCount = 0;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mAmountTextView;
        public TextView mDateTextView;
        public TextView mDistanceTextView;
        public ImageView mShare;
        private List<Donation> mDonations;
        private static final String TAG = ViewHolder.class.getSimpleName();

        public ViewHolder(final View view, List<Donation> donationList) {
            super(view);
            mDonations = donationList;
            final Context context = view.getContext();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(context, DonationDetailActivity.class);
                    intent.putExtra(DonationDetailActivity.EXTRA_DONATION_PARCELABLE,
                            mDonations.get(position));
                    view.getContext().startActivity(intent);
                }
            });
            mAmountTextView = (TextView) view.findViewById(R.id.amount_text_view);
            mDateTextView = (TextView) view.findViewById(R.id.date_text_view);
            mDistanceTextView = (TextView) view.findViewById(R.id.distance_text_view);
            mShare = (ImageView) view.findViewById(R.id.card_share);
            mShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = view.getContext();
                    Donation donation = mDonations.get(getAdapterPosition());
                    String message = StringGenerator.getDonationShareMessage(context, donation);
                    try {
                        /* FIXME: If uri exists, but file the doesn't, we won't attach any image */
                        Uri imageUri = DonationUtils.getImageForDonation(context, donation);
                        if (imageUri == null) {
                            imageUri = DonationUtils.getDefaultImage(context);
                        }
                        SocialShareUtils.shareData(context, imageUri, message);
                    } catch (IOException e) {
                        Toast.makeText(context, R.string.toast_share_error, Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            });
        }
    }

    public DonationsListAdapter(List<Donation> donations) {
        mDonations = donations;
    }

    @Override
    public DonationsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        mContext = parent.getContext();
        mParentWidth = parent.getWidth();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.donation_item_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, mDonations);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Donation donation = mDonations.get(position);

        String amount = StringGenerator.moneyToText(mContext, donation.getAmount());
        holder.mAmountTextView.setText(amount);

        String formattedDate = StringGenerator.timestampToShortDateTimeText(mContext,
                donation.getTimestamp());
        holder.mDateTextView.setText(formattedDate);

        String formatDistance = StringGenerator.distanceToText(mContext, donation.getDistance());
        holder.mDistanceTextView.setText(formatDistance);

        if (mAnimateCards) {
            holder.itemView.setTranslationX(-mParentWidth);
            mAnimationCount++;
            holder.itemView.animate().setStartDelay((mAnimationCount - 1) * 100).translationX(0)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mAnimationCount--;
                            if (mAnimationCount == 0) {
                                mAnimateCards = false;
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        }
    }

    @Override
    public int getItemCount() {
        return mDonations.size();
    }

    public void animationEnabled(boolean enabled) {
        mAnimateCards = enabled;
    }
}

