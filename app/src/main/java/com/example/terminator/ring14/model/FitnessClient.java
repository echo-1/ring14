package com.example.terminator.ring14.model;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.terminator.ring14.utils.PermissionsUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.location.LocationServices;

/**
 * This class creates an object to interact with Google Fit.
 */
public class FitnessClient {

    /**
     * Provides callback that is called when the client is connected from the service
     */
    public interface Connection {
        /**
         * After calling {@link #connect()}, this method will be invoked asynchronously when
         * the connect request has successfully completed.
         */
        void onConnected();
    }

    private static final String TAG = FitnessClient.class.getSimpleName();

    private GoogleApiClient mClient = null;

    private Connection mConnectionCallback = null;

    /**
     * Returns a FitnessClient object making sure that all the requirements (e.g. permissions)
     * are satisfied.
     *
     * @param context Context used to build the permission dialog
     * @param connection Connection object to pass async results to the caller
     */
    public FitnessClient(Context context, final Connection connection) {
        if (mClient == null && PermissionsUtils.checkLocationPermission(context)) {
            mConnectionCallback = connection;
            mClient = buildBaseClient(context)
                    .addOnConnectionFailedListener(
                            new GoogleApiClient.OnConnectionFailedListener() {
                                @Override
                                public void onConnectionFailed(@NonNull ConnectionResult result) {
                                    Log.e(TAG, "Failed connection " + result.getErrorMessage());
                                }
                            }
                    )
                    .build();
        }
    }

    private GoogleApiClient.Builder buildBaseClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.RECORDING_API)
                .addApi(Fitness.SENSORS_API)
                .addApi(Fitness.SESSIONS_API)
                .addApi(LocationServices.API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
                .addConnectionCallbacks(
                        new GoogleApiClient.ConnectionCallbacks() {
                            @Override
                            public void onConnected(Bundle bundle) {
                                Log.d(TAG, "Connected!!!");
                                mConnectionCallback.onConnected();
                            }

                            @Override
                            public void onConnectionSuspended(int i) {
                                if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
                                    Log.d(TAG, "Connection lost.  Cause: Network Lost.");
                                } else if (i
                                        == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
                                    Log.d(TAG,
                                            "Connection lost.  Reason: Service Disconnected");
                                }
                            }
                        }
                );
    }

    public void setConnectionCallback(Connection connectionCallback) {
        mConnectionCallback = connectionCallback;
    }

    /**
     * Connect this FitnessClient (i.e. connect the {@link GoogleApiClient})
     */
    public void connect() {
        mClient.connect();
    }

    /**
     * Disconnect this FitnessClient (i.e. connect the {@link GoogleApiClient}).
     * This method can be called even if the client is not yet connected.
     */
    public void disconnect() {
        if (mClient.isConnected()) {
            mClient.disconnect();
        }
    }

    /**
     * @return {@link GoogleApiClient} associated to this FitnessClient.
     */
    public GoogleApiClient getGoogleApiClient() {
        return mClient;
    }
}
