package com.example.terminator.ring14.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.example.terminator.ring14.R;

public class PermissionsUtils {

    /**
     * Returns whether {@link android.Manifest.permission#ACCESS_FINE_LOCATION} was granted or not.
     */
    public static boolean checkLocationPermission(Context context) {
        int permissionState = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Asks the user for {@link android.Manifest.permission#ACCESS_FINE_LOCATION} or returns true
     * if already granted.
     */
    public static boolean checkAndRequestLocationPermission(final Activity activity,
                                                            final int requestCode) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    requestCode);
            return false;
        }
    }

    /**
     * Show dialog about the permission requirements and offer option to exit.
     */
    public static void requestLocationPermissionOrExitDialog(final Activity activity,
                                                             final int requestCode) {
        DialogInterface.OnClickListener buttonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        checkAndRequestLocationPermission(activity, requestCode);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        activity.finish();
                        break;
                }
            }
        };

        new AlertDialog.Builder(activity)
                .setMessage(R.string.location_permission_info)
                .setPositiveButton(android.R.string.ok, buttonListener)
                .setNegativeButton(android.R.string.cancel, buttonListener)
                .create()
                .show();
    }

    public static boolean checkStoragePermission(Context context) {
        int permissionState = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkAndRequestStoragePermission(final Activity activity,
            final int requestCode) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    requestCode);
            return false;
        }
    }

    public static void requestStoragePermission(final Activity activity,
            final int requestCode) {
        DialogInterface.OnClickListener buttonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        checkAndRequestStoragePermission(activity, requestCode);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        new AlertDialog.Builder(activity)
                .setTitle(R.string.permission_denied)
                .setMessage(R.string.permission_photo_rationale)
                .setPositiveButton(android.R.string.ok, buttonListener)
                .setNegativeButton(android.R.string.cancel, buttonListener)
                .create()
                .show();
    }
}
