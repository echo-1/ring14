package com.example.terminator.ring14.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class BitmapUtils {

    public interface BitmapResizeCallback {
        /**
         * Called when the resized and compressed bitmap is ready
         *
         * @param bitmap Resized and compressed bitap
         */
        void onBitmapResized(Bitmap bitmap);
    }

    public static final int MAX_BITMAP_DIM_DEFAULT = 1280;

    public static final int JPEG_QUALITY_DEFAULT = 80;

    private static final String TAG = Bitmap.class.getSimpleName();

    /**
     * Creates a resized bitmap such that both the width and height are smaller or equal than
     * the given size. If the width and height are already smaller than the given size, the
     * source bitmap is returned and no new Bitmap is created.
     *
     * @param source The source bitmap
     * @param maxDim The new bitmap's max size
     * @return The resized bitmap or the source bitmap
     */
    public static Bitmap resizeBitmap(Bitmap source, int maxDim) {
        if (source.getWidth() > maxDim || source.getHeight() > maxDim) {
            float aspectRatio = (float) source.getHeight() / (float) source.getWidth();
            int targetHeight, targetWidth;
            if (aspectRatio > 1) {
                targetHeight = maxDim;
                targetWidth = Math.round(maxDim / aspectRatio);
            } else {
                targetHeight = Math.round(maxDim / aspectRatio);
                targetWidth = maxDim;
            }
            Bitmap resized = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
            Log.d(TAG, "Resized: w=" + resized.getWidth() + " h=" + resized.getHeight());
            source = resized;
        }
        return source;
    }

    /**
     * Resize and compress the given bitmap asynchronously.
     * The given bitmap is destroyed.
     *
     * @param source Source bitmap to resize and compress
     * @param callback A callback that will be executed when the compressed image is ready
     * @return The AsyncTask that compresses and resizes the bitmap
     */
    public static AsyncTask resizeAndCompress(Bitmap source, BitmapResizeCallback callback) {

        class ResizeAndCompressAsync extends AsyncTask<Object, Void, Bitmap> {

            private BitmapResizeCallback mCallback;

            @Override
            protected Bitmap doInBackground(Object... params) {
                Bitmap source = (Bitmap) params[0];
                int maxDim = (int) params[1];
                int quality = (int) params[2];
                mCallback = (BitmapResizeCallback) params[3];

                Bitmap resizedBitmap = BitmapUtils.resizeBitmap(source, maxDim);

                // if the two referenced areas of memory are different then free the original one
                if (source != resizedBitmap) {
                    source.recycle();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos);
                ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
                return BitmapFactory.decodeStream(bais);
            }

            @Override
            protected void onPostExecute(Bitmap result) {
                mCallback.onBitmapResized(result);
            }
        }

        return new ResizeAndCompressAsync().execute(source, MAX_BITMAP_DIM_DEFAULT,
                JPEG_QUALITY_DEFAULT, callback);
    }

    /**
     * Resize and compress the given bitmap asynchronously.
     *
     * @param context A context to retrieve the bitmap by Uri
     * @param bitmapUri Uri of the bitmpage
     * @param callback A callback that will be executed when the compressed image is ready
     * @return The AsyncTask that compresses the bitmap
     * @throws IOException
     */
    public static AsyncTask resizeAndCompress(Context context, Uri bitmapUri,
                                              BitmapResizeCallback callback) throws IOException {
        Bitmap source = MediaStore.Images.Media.getBitmap(context.getContentResolver(), bitmapUri);
        return resizeAndCompress(source, callback);
    }

    public static File saveImage(File filepath, Bitmap bitmap)
            throws IOException {
        File tempFile = new File(filepath.getAbsolutePath() + ".tmp");
        try {
            FileOutputStream out = new FileOutputStream(tempFile, false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Could not write to " + filepath.getName() + ".tmp");
            throw e;
        }

        try {
            boolean success = true;
            if (filepath.exists()) {
                success = filepath.delete();
            }
            if (!success) {
                throw new IOException("Could not delete original file");
            }
            success = tempFile.renameTo(filepath);
            if (!success) {
                throw new IOException("Could not save new image");
            }
        } catch (IOException e) {
            tempFile.delete();
            throw e;
        }
        return filepath;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmap(Context context, Uri uri,
            int reqWidth, int reqHeight) throws IOException {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream is = context.getContentResolver().openInputStream(uri);
        BitmapFactory.decodeStream(is, null, options);
        is.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        is = context.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);
        is.close();

        return bitmap;
    }
}
