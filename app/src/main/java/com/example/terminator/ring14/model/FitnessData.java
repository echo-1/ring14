package com.example.terminator.ring14.model;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.DataDeleteRequest;
import com.google.android.gms.fitness.request.SessionReadRequest;
import com.google.android.gms.fitness.result.SessionReadResult;
import com.google.android.gms.fitness.result.SessionStopResult;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * This class creates an object to work with Google Fit sessions.
 */
public class FitnessData {

    /**
     * Provides callbacks that are called when the client is connected from the service
     */
    public static abstract class ReadCallback {
        /**
         * After calling {@link #getSession(String, ReadCallback)}, this method will be invoked
         * asynchronously when the data is available.
         *
         * @param session Requested session
         * @param dataSets Data associated with the returned session
         */
        public abstract void onDataReady(Session session, List<DataSet> dataSets);

        /**
         * After calling {@link #getSession(String, ReadCallback)}, this method will be invoked
         * asynchronously if not data was found.
         * the connect request has successfully completed.
         */
        public abstract void onDataNotFound();
    }

    private static final String TAG = FitnessData.class.getSimpleName();

    private GoogleApiClient mClient = null;

    /**
     * Returns a FitnessData object. The given {@code googleApiClient} must have the permissions
     * to use Fitness.SESSIONS_API and Fitness.HISTORY_API.
     *
     * @param googleApiClient GoogleApiClient instance to use
     */
    public FitnessData(GoogleApiClient googleApiClient) {
        mClient = googleApiClient;
    }

    /**
     * Returns a FitnessData object.
     */
    public FitnessData(FitnessClient client) {
        mClient = client.getGoogleApiClient();
    }

    private long getTimeBegin() {
        Calendar cal = Calendar.getInstance();
        cal.set(2015, 1, 1);
        return cal.getTimeInMillis();
    }

    private long getTimeEnd() {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        return cal.getTimeInMillis();
    }

    private void processReadRequest(SessionReadRequest readRequest,
                                    final ResultCallback<SessionReadResult> callback) {
        Fitness.SessionsApi.readSession(mClient, readRequest)
                .setResultCallback(new ResultCallback<SessionReadResult>() {
                    @Override
                    public void onResult(@NonNull SessionReadResult sessionReadResult) {
                        callback.onResult(sessionReadResult);
                    }
                });
    }

    private void processReadRequestSingle(SessionReadRequest readRequest,
                                          final ReadCallback readCallback) {
        processReadRequest(readRequest, new ResultCallback<SessionReadResult>() {
            @Override
            public void onResult(@NonNull SessionReadResult sessionReadResult) {
                boolean found = sessionReadResult.getSessions().size() != 0;
                if (found) {
                    Session session = sessionReadResult.getSessions().get(0);
                    List<DataSet> dataSets = sessionReadResult.getDataSet(session);
                    readCallback.onDataReady(session, dataSets);
                } else {
                    readCallback.onDataNotFound();
                }
            }
        });
    }

    /**
     * Get Fit session by ID.
     *
     * @param sessionId ID of the session to fetch
     * @param readCallback CallbackObject used once the result is available
     */
    public void getSession(final String sessionId,
                           final ReadCallback readCallback) {
        SessionReadRequest.Builder sessionReadRequest = new SessionReadRequest.Builder()
                .setTimeInterval(getTimeBegin(), getTimeEnd(), TimeUnit.MILLISECONDS)
                .enableServerQueries()
                .setSessionId(sessionId);
        for (DataType dataType : FitnessSensors.getDataTypes()) {
            sessionReadRequest.read(dataType);
        }
        processReadRequestSingle(sessionReadRequest.build(), readCallback);
    }

    /**
     * Delete Fit session by ID. If the session with ID equals to {@code sessionId} is ongoing, the
     * session will be first stopped and then deleted.
     *
     * This method will silently fail if no session with the given ID is found.
     *
     * @param sessionId ID of the session to delete
     */
    public void deleteSession(final String sessionId) {
        getSession(sessionId, new ReadCallback() {
            @Override
            public void onDataReady(Session session, List<DataSet> dataSets) {
                deleteSession(session);
            }

            @Override
            public void onDataNotFound() {
                Log.w(TAG, "Session with id " + sessionId + " was not found");
            }
        });
    }

    /**
     * Delete given Fit Session.
     * If the given session is ongoing, it is first stopped and then deleted.
     *
     * @param session session to delete
     */
    public void deleteSession(final Session session) {
        if (session.isOngoing()) {
            /*
             * Sessions can be deleted only if stopped. If for some reason a session is still
             * ongoing, stop it and then delete it.
             */
            Fitness.SessionsApi.stopSession(mClient, session.getIdentifier())
                    .setResultCallback(new ResultCallbacks<SessionStopResult>() {
                        @Override
                        public void onSuccess(@NonNull SessionStopResult sessionStopResult) {
                            Log.d(TAG, "Successfully stopped session " + session.getIdentifier());
                            /* Fetch session by ID to have an updated Session object */
                            deleteSession(session.getIdentifier());
                        }

                        @Override
                        public void onFailure(@NonNull Status status) {
                            Log.e(TAG, "Failed to stop " + session.getIdentifier());
                        }
                    });
            return;
        }

        DataDeleteRequest request = new DataDeleteRequest.Builder()
                .setTimeInterval(getTimeBegin(), getTimeEnd(), TimeUnit.MILLISECONDS)
                .addSession(session)
                .build();

        Fitness.HistoryApi.deleteData(mClient, request)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            Log.d(TAG, "Successfully deleted " + session.getIdentifier());
                        } else {
                            Log.e(TAG, "Failed to delete " + session.getIdentifier());
                        }
                    }
                });
    }

}
