package com.example.terminator.ring14.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.terminator.ring14.Constants;
import com.example.terminator.ring14.R;
import com.example.terminator.ring14.activities.DonationActivity;
import com.example.terminator.ring14.activities.MainActivity;
import com.example.terminator.ring14.adapters.DonationsListAdapter;
import com.example.terminator.ring14.interfaces.FirebaseFragment;
import com.example.terminator.ring14.utils.DonationUtils;
import com.example.terminator.ring14.model.Donation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DonationsListFragment extends Fragment implements FirebaseFragment {

    private static final String TAG = DonationsListFragment.class.getSimpleName();

    private static final int RC_DONATION = 1033;

    private static final boolean TEST_EMPTY_LIST = false;
    private boolean mEmptyTestDonated = false;

    private static final String KEY_DONATION_LIST = "donation_list";

    private RecyclerView mRecyclerView;

    private DonationsListAdapter mAdapter;

    private View mNoDonationsContainer;

    private RecyclerView.LayoutManager mLayoutManager;

    private List<Donation> mDonations = new ArrayList<>();

    public static DonationsListFragment newInstance() {
        return new DonationsListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donations_list, container, false);
        setHasOptionsMenu(true);

        final MainActivity activity = (MainActivity) getActivity();
        activity.setTitle(R.string.donationlist_frag_title);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (savedInstanceState == null) {
            activity.showLoadingAnimation(true);
            if (activity.isAuthenticated()) {
                fetchDonations();
            }
        } else {
            mDonations = savedInstanceState.getParcelableArrayList(KEY_DONATION_LIST);
        }

        mAdapter = new DonationsListAdapter(mDonations);
        mAdapter.animationEnabled(savedInstanceState == null);
        mRecyclerView.setAdapter(mAdapter);

        mNoDonationsContainer = view.findViewById(R.id.no_donations_container);

        activity.setFabAction(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, DonationActivity.class);
                startActivityForResult(intent, RC_DONATION);
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                activity.showFab(dy <= 0);
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.donation_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.amount_asc:
                orderAdapterBy(DonationUtils.getAmountComparator());
                break;
            case R.id.amount_dsc:
                orderAdapterBy(Collections.reverseOrder(DonationUtils.getAmountComparator()));
                break;
            case R.id.distance_asc:
                orderAdapterBy(DonationUtils.getDistanceComparator());
                break;
            case R.id.distance_dsc:
                orderAdapterBy(Collections.reverseOrder(DonationUtils.getDistanceComparator()));
                break;
            case R.id.date_asc:
                orderAdapterBy(DonationUtils.getDateComparator());
                break;
            case R.id.date_dsc:
                orderAdapterBy(Collections.reverseOrder(DonationUtils.getDateComparator()));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(KEY_DONATION_LIST,
                (ArrayList<Donation>) mDonations);
        super.onSaveInstanceState(outState);
    }

    private void fetchDonations() {
        final MainActivity activity = (MainActivity) getActivity();
        String uid = activity.getCurrentUser().getUid();
        DonationUtils.getUserDonations(activity.getFirebaseConnection(), uid,
                new DonationUtils.MultiValueCallback() {

                    @Override
                    public void onDataChange(List<Donation> donations) {
                        // Don't do anything if this fragment is no longer attached to its activity
                        if (!isAdded()) {
                            return;
                        }

                        mDonations.clear();
                        for (Donation donation : donations) {
                            if (donation.isValid()) {
                                mDonations.add(donation);
                            }
                        }
                        if (Constants.LOCAL_TEST && mDonations.size() == 0) {
                            mDonations.add(DonationUtils.getTestDonation());
                        }
                        if (TEST_EMPTY_LIST && !mEmptyTestDonated) {
                            mEmptyTestDonated = true;
                            mDonations.clear();
                        }

                        if (mDonations.size() == 0) {
                            mNoDonationsContainer.setVisibility(View.VISIBLE);
                        }

                        activity.showLoadingAnimation(false);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled() {
                        Toast.makeText(activity, "Could not load donation list",
                                Toast.LENGTH_SHORT).show();
                        activity.showLoadingAnimation(false);
                    }
                });
    }

    @Override
    public void onAuthenticated() {
        if (getView() == null) {
            /* This method can be called before onCreateView() */
            return;
        }
        fetchDonations();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_DONATION) {
            if (resultCode == Activity.RESULT_OK) {
                mNoDonationsContainer.setVisibility(View.GONE);
                fetchDonations();

                String donationId = data.getStringExtra(DonationActivity.DONATION_ID_EXTRA);
                ((MainActivity) getActivity()).showSnackbarFor(donationId);
            }
        }
    }

    private void orderAdapterBy(Comparator<Donation> comparator){
        Collections.sort(mDonations, comparator);
        mRecyclerView.scrollToPosition(0);
        mAdapter.animationEnabled(true);
        mAdapter.notifyDataSetChanged();
    }
}
