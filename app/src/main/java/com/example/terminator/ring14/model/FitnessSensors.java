package com.example.terminator.ring14.model;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FitnessSensors {

    private static final String TAG = FitnessSensors.class.getSimpleName();

    private static final int INTERVAL_MS = 5000;

    private static final DataType DATA_TYPES[] = {
            DataType.TYPE_LOCATION_SAMPLE,
            DataType.TYPE_DISTANCE_DELTA,
            DataType.TYPE_SPEED,
    };

    public interface DataPointListener {
        void onDataPoint(DataPoint dataPoint);
    }
    private DataPointListener mDataPointListener;

    private List<OnDataPointListener> mListeners = new ArrayList<>();

    private GoogleApiClient mGoogleApiClient;

    public FitnessSensors(GoogleApiClient googleApiClient, DataPointListener dataPointListener) {
        mGoogleApiClient = googleApiClient;
        mDataPointListener = dataPointListener;
    }

    private void registerFitnessDataListener(final DataType dataType) {

        OnDataPointListener listener = new OnDataPointListener() {
            @Override
            public void onDataPoint(DataPoint dataPoint) {
                mDataPointListener.onDataPoint(dataPoint);
            }
        };
        mListeners.add(listener);

        Fitness.SensorsApi.add(
                mGoogleApiClient,
                new SensorRequest.Builder()
                        .setDataType(dataType)
                        .setSamplingRate(INTERVAL_MS, TimeUnit.MILLISECONDS)
                        .build(),
                listener)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                            Log.d(TAG, "Listener registered, dataType: " + dataType);
                        } else {
                            Log.e(TAG, "Listener not registered, dataType: " + dataType);
                        }
                    }
                });
    }

    public void unregisterFitnessDataListener() {
        if (!mGoogleApiClient.isConnected()) {
            Log.d(TAG, "FitnessClient not connected");
            return;
        }

        for (OnDataPointListener listener : mListeners) {
            Fitness.SensorsApi.remove(
                    mGoogleApiClient,
                    listener)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            if (status.isSuccess()) {
                                Log.d(TAG, "Listener was removed!");
                            } else {
                                Log.e(TAG, "Listener was not removed.");
                            }
                        }
                    });
        }

        mListeners.clear();
    }

    public void findFitnessDataSources() {
        for (DataType dataType : getDataTypes()) {
            registerFitnessDataListener(dataType);
        }
    }

    public static List<DataType> getDataTypes() {
        return Arrays.asList(DATA_TYPES);
    }
}
