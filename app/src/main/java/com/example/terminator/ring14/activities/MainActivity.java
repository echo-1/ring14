package com.example.terminator.ring14.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.terminator.ring14.R;
import com.example.terminator.ring14.fragments.DonationsListFragment;
import com.example.terminator.ring14.fragments.OverviewFragment;
import com.example.terminator.ring14.interfaces.FirebaseFragment;
import com.example.terminator.ring14.model.Donation;
import com.example.terminator.ring14.services.FitnessService;
import com.example.terminator.ring14.utils.DonationUtils;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.auth.FirebaseUser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int RC_INVITE = 1032;
    private static final int RC_GO_TO_DONATION = 1034;

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String SHARED_PREF = "user_details";
    private static final String PREF_USER_NAME = "user_name";
    private static final String PREF_USER_EMAIL = "user_email";
    private static final String USER_PIC_FILENAME = "user_pic";

    private static final String KEY_SELECTED_MENU_ITEM = "selected_menu_item";
    private static final String KEY_SETUP_DONE = "setup_done";

    private FloatingActionButton mFab;

    private int mCurMenuItem;

    private View mNavigationHeader;

    private boolean mSetup = false;

    private AsyncHttpClient mHttpClient;

    private boolean mTabletLayout = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* If the app was killed while the service was running, KEY_ACTIVE is true */
        SharedPreferences sharedPref =
                getSharedPreferences(FitnessService.PREF_NAME, MODE_PRIVATE);
        boolean isServiceRunning = sharedPref.getBoolean(FitnessService.KEY_ACTIVE, false);
        if (isServiceRunning) {
            startActivity(new Intent(this, MapsActivity.class));
        }

        mTabletLayout = getResources().getBoolean(R.bool.is_tablet);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFab = (FloatingActionButton) findViewById(R.id.fab);

        if (!mTabletLayout) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close);

            // Tie DrawerLayout events to the ActionBarToggle
            drawer.addDrawerListener(toggle);
            toggle.syncState();
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mNavigationHeader = navigationView.getHeaderView(0);

        mHttpClient = new AsyncHttpClient();

        updateUserDetails();

        if (savedInstanceState == null) {
            /* Don't change fragment on screen orientation change */
            updateMainContainer(OverviewFragment.newInstance());

            mCurMenuItem = navigationView.getMenu().getItem(0).getItemId();
            navigationView.setCheckedItem(mCurMenuItem);
        } else {
            mCurMenuItem = savedInstanceState.getInt(KEY_SELECTED_MENU_ITEM);
            navigationView.setCheckedItem(mCurMenuItem);
            mSetup = savedInstanceState.getBoolean(KEY_SETUP_DONE, false);
        }
    }

    @Override
    public void onDestroy() {
        mHttpClient.cancelAllRequests(true);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        /* Needed for when we return from an activity started from the NavigationView. */
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(mCurMenuItem);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_SELECTED_MENU_ITEM, mCurMenuItem);
        outState.putBoolean(KEY_SETUP_DONE, mSetup);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (mTabletLayout) {
            super.onBackPressed();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void updateMainContainer(Fragment fragment) {
        showFab(true);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_container, fragment);
        fragmentTransaction.commit();
    }

    private void updateMainContainer(Fragment fragment, int menuItemId) {
        updateMainContainer(fragment);
        mCurMenuItem = menuItemId;
    }

    private void signOutAccount() {
        clearUserData();
        signOut();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        final int id = item.getItemId();

        if (id == mCurMenuItem) {
            return false;
        }

        if (id == R.id.nav_overview) {
            updateMainContainer(OverviewFragment.newInstance(), id);
        } else if (id == R.id.nav_account) {
            updateMainContainer(DonationsListFragment.newInstance(), id);
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(this, PreferenceActivity.class));
        } else if (id == R.id.nav_send) {
            sendInvitation();
        } else if (id == R.id.nav_logout) {
            signOutAccount();
            return false;
        } else {
            return false;
        }

        if (!mTabletLayout) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }

        return true;
    }

    @Override
    protected void onFirebaseAuthenticated() {
        if (!mSetup) {
            updateUserDetails(getCurrentUser());
            mSetup = true;

            /*
             * The first fragment is usually created before the authentication has been completed.
             * If the fragment uses remote data, it either uses cached data or waits indefinitely,
             * so force an update as soon as we are ready.
             */
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentById(R.id.main_container);
            if (fragment instanceof FirebaseFragment) {
                ((FirebaseFragment)(fragment)).onAuthenticated();
            }
        }
    }

    @Override
    protected void onFirebaseAuthenticationFailure() {
    }

    public void showLoadingAnimation(boolean show) {
        findViewById(R.id.loading_animation).setVisibility(show ? View.VISIBLE : View.GONE);
        findViewById(R.id.main_container).setVisibility(!show ? View.VISIBLE : View.GONE);

        /* Hide the fab while showing the animation */
        showFab(!show);
    }

    public void setFabAction(View.OnClickListener onClickListener) {
        mFab.setOnClickListener(onClickListener);
    }

    public void showFab(boolean show) {
        if (show) {
            mFab.show();
        } else {
            mFab.hide();
        }
    }

    private void setUserName(String userName) {
        TextView userNameText = (TextView) mNavigationHeader.findViewById(R.id.user_name);
        userNameText.setText(userName);
    }

    private void setUserEmail(String userEmail) {
        TextView userEmailText = (TextView) mNavigationHeader.findViewById(R.id.user_email);
        userEmailText.setText(userEmail);
    }

    private void setUserImage(BitmapDrawable image) {
        CircleImageView circleImageView =
                (CircleImageView) mNavigationHeader.findViewById(R.id.user_image);
        circleImageView.setImageDrawable(image);
    }

    private void updateUserDetails(FirebaseUser user) {
        String userName = user.getDisplayName();
        setUserName(userName);
        String userEmail = user.getEmail();
        setUserEmail(userEmail);

        if (user.getPhotoUrl() != null) {
            final String imageUrl = user.getPhotoUrl().toString();
            final File userPic = new File(getCacheDir(), USER_PIC_FILENAME);
            mHttpClient.get(this, imageUrl, new FileAsyncHttpResponseHandler(userPic) {
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable t, File file) {
                    Log.w(TAG, "Could not download user image at " + imageUrl);
                    /* Make sure we don't leave a useless file */
                    userPic.delete();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, File response) {
                    Bitmap bitmap = BitmapFactory.decodeFile(response.getAbsolutePath());
                    if (bitmap == null) {
                        Log.d(TAG, "Could not find a valid user picture");
                        response.delete();
                    }
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
                    setUserImage(bitmapDrawable);
                }
            });
        }

        SharedPreferences sharedPref = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.putString(PREF_USER_EMAIL, userEmail);
        editor.apply();
    }

    private void clearUserData() {
        File userPic = new File(getCacheDir(), USER_PIC_FILENAME);
        userPic.delete();
        PreferenceManager.getDefaultSharedPreferences(this).edit().clear().apply();
        getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE).edit().clear().apply();
        getSharedPreferences(FitnessService.PREF_NAME, MODE_PRIVATE).edit().clear().apply();
    }

    private void updateUserDetails() {
        SharedPreferences sharedPref = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        String userName = sharedPref.getString(PREF_USER_NAME, "");
        setUserName(userName);
        String userEmail = sharedPref.getString(PREF_USER_EMAIL, "");
        setUserEmail(userEmail);

        try {
            File cachedPic = new File(getCacheDir(), USER_PIC_FILENAME);
            FileInputStream inputStream = new FileInputStream(cachedPic);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
            setUserImage(bitmapDrawable);
        } catch (FileNotFoundException e) {
            Log.d(TAG, "No cached user pic found");
        }
    }

    //Sends invitation to use this app
    // https://firebase.google.com/docs/invites/android#receive-invitations
    private void sendInvitation() {
        showLoadingAnimation(true);
        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setCallToActionText(getString(R.string.invitation_cta))
                .build();
        startActivityForResult(intent, RC_INVITE);
    }

    private void startDonationDetailActivity(String donationId) {
        showLoadingAnimation(true);
        DonationUtils.getDonation(getFirebaseConnection(), donationId,
                new DonationUtils.SingleValueCallback() {

            @Override
            public void onDataChange(Donation donation) {
                Intent i = new Intent(MainActivity.this, DonationDetailActivity.class);
                i.putExtra(DonationDetailActivity.EXTRA_DONATION_PARCELABLE, donation);
                startActivityForResult(i, RC_GO_TO_DONATION);
            }

            @Override
            public void onCancelled() {
                showLoadingAnimation(false);
                Toast.makeText(MainActivity.this, R.string.toast_donatioin_load_error,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == RC_INVITE) {
            showLoadingAnimation(false);
            if (resultCode == RESULT_OK) {
                // Check how many invitations were sent.
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                Log.d(TAG, "Invitations sent: " + ids.length);
            } else {
                // Sending failed or it was canceled, show failure message to
                // the user
                Log.e(TAG, "Failed to send invitation.");
            }
        } else if (requestCode == RC_GO_TO_DONATION) {
            showLoadingAnimation(false);
        }
    }

    public void showSnackbarFor(final String donationId) {
        View view = findViewById(R.id.coordinator_app_bar_main);
        final Snackbar snackbar = Snackbar.make(view, R.string.payment_went_through,
                Snackbar.LENGTH_LONG);

        if (donationId != null) {
            snackbar.setAction(R.string.show_donation_snackbar, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startDonationDetailActivity(donationId);
                }
            });
        } else {
            snackbar.setAction(android.R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
        }
        snackbar.show();
    }
}
