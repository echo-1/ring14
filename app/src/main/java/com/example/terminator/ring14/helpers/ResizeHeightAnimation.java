package com.example.terminator.ring14.helpers;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeHeightAnimation extends Animation {

    private final int mStartHeight;
    private final int mTargetHeight;
    private final View mView;

    public ResizeHeightAnimation(View view, int targetHeight) {
        mView = view;
        mTargetHeight = targetHeight;
        mStartHeight = view.getHeight();
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        mView.getLayoutParams().height =
                (int) (mStartHeight + (mTargetHeight - mStartHeight) * interpolatedTime);
        mView.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
