package com.example.terminator.ring14.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.example.terminator.ring14.R;
import com.example.terminator.ring14.utils.DonationUtils;
import com.example.terminator.ring14.utils.StringGenerator;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import java.io.IOException;

public class LoginActivity extends BaseClientActivity implements View.OnClickListener {

    private static final int RC_AUTH = 1;
    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.login_button).setOnClickListener(this);
        buttonEnabled(false);
        checkFirstRun();
        checkLocalePreference();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                showSpinner(true);
                signInWithGoogle();
                break;
        }
    }

    private void buttonEnabled(boolean enabled) {
        findViewById(R.id.login_button).setEnabled(enabled);
    }

    @Override
    public void onSignInFailed() {
        buttonEnabled(false);
    }

    @Override
    public void onSignInSucceeded() {
        buttonEnabled(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr =
                Auth.GoogleSignInApi.silentSignIn(getApiClient());
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            showSpinner(true);
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    public void signInWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(getApiClient());
        startActivityForResult(signInIntent, RC_AUTH);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_AUTH) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void checkFirstRun() {

        final String FIRST_RUN_PREF = "first_run";
        final String PREF_VERSION_CODE_KEY = "version_code";
        final int DOESNT_EXIST = -1;


        // Get current version code
        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            // handle exception
            e.printStackTrace();
            return;
        }

        SharedPreferences prefs = getSharedPreferences(FIRST_RUN_PREF, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        // Check for first run or upgrade
        if (currentVersionCode == savedVersionCode) {

            // This is just a normal run
            return;

        } else if (currentVersionCode > savedVersionCode || savedVersionCode == DOESNT_EXIST) {
            try {
                DonationUtils.saveDefaultImage(this);
                // Update the shared preferences with the current version code
                prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();
            } catch (IOException e){
                Log.e(TAG, "Couldn't save the dafault image on first run", e);
            }
        }
    }

    private void checkLocalePreference() {
        /* Set a default unit system depending on the locale of the first start and set it. */
        SharedPreferences unitPref = PreferenceManager.getDefaultSharedPreferences(this);
        String prefKey = getString(R.string.key_unit_pref);
        if (!unitPref.contains(prefKey)) {
            String defaultSystem = StringGenerator.getDefaultUnitSystemValue(this);
            unitPref.edit().putString(prefKey, defaultSystem).apply();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            loginDone();
        } else {
            showSpinner(false);
        }
    }

    private void loginDone() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void showSpinner(boolean show) {
        findViewById(R.id.login_button).setVisibility(show ? View.GONE : View.VISIBLE);
        findViewById(R.id.loading_animation).setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
