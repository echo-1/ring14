package com.example.terminator.ring14.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.example.terminator.ring14.R;

public class SocialShareUtils {

    public static void shareData(Context context, Uri imageUri, String message) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        shareIntent.setType("image/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        String title = context.getString(R.string.intent_share_title);
        // Create intent to show the chooser dialog
        Intent chooser = Intent.createChooser(shareIntent, title);
        // Verify the original intent will resolve to at least one activity
        ComponentName componentName = shareIntent.resolveActivity(context.getPackageManager());
        if (componentName != null) {
            String packageName = componentName.getPackageName();
            context.grantUriPermission(packageName, imageUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(chooser);
        }
    }

}
