package com.example.terminator.ring14.model;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.result.SessionStopResult;

import java.util.concurrent.TimeUnit;

/**
 * This class creates an object to record Google Fit sessions.
 */
public class FitnessRecording {

    private static final String TAG = FitnessRecording.class.getSimpleName();

    private GoogleApiClient mGoogleApiClient;

    private Session mSession;

    /**
     * Returns a FitnessRecording object. The given {@code googleApiClient} must have the
     * permissions to use {@link Fitness#RECORDING_API} and {@link Fitness#SESSIONS_API}.
     *
     * @param googleApiClient GoogleApiClient instance to use
     */
    public FitnessRecording(GoogleApiClient googleApiClient) {
        mGoogleApiClient = googleApiClient;
    }

    private void subscribe() {
        for (final DataType type : FitnessSensors.getDataTypes()) {
            Log.d(TAG, "Subscribing to data type: " + type);
            Fitness.RecordingApi.subscribe(mGoogleApiClient, type)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            if (status.isSuccess()) {
                                if (status.getStatusCode()
                                        == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
                                    Log.d(TAG, "Existing subscription for activity detected.");
                                } else {
                                    Log.d(TAG, "Successfully subscribed to data type: " + type);
                                }
                            } else {
                                Log.e(TAG, "There was a problem subscribing to data type " + type);
                            }
                        }
                    });
        }
    }

    private void cancelSubscription() {
        for (final DataType type : FitnessSensors.getDataTypes()) {
            Log.d(TAG, "Unsubscribing from data type: " + type);
            Fitness.RecordingApi.unsubscribe(mGoogleApiClient, type)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            if (status.isSuccess()) {
                                Log.d(TAG, "Successfully unsubscribed for data type: " + type);
                            } else {
                                Log.e(TAG, "Failed to unsubscribe for data type: " + type);
                            }
                        }
                    });
        }
    }

    /**
     * Starts recording a new session. See
     * {@link com.google.android.gms.fitness.SessionsApi#startSession(GoogleApiClient, Session)}
     * for more details.
     *
     * @param sessionId ID of the session
     * @param sessionName Name of the session
     * @param sessionDescription  Description of the session
     * @param fitnessActivity Activity of the session
     * @param startTime Start time of the session
     */
    public void startSession(final String sessionId, final String sessionName,
                             final String sessionDescription, final String fitnessActivity,
                             final long startTime) {
        if (mSession != null && mSession.isOngoing()) {
            Log.e(TAG, "Session is ongoing");
            return;
        }

        subscribe();

        /*
         * NOTE: if we use TimeUnit.MILLISECONDS along with System.currentTimeMillis()
         * we randomly get the following error:
         * java.lang.IllegalArgumentException: Cannot start a session in the future
         */
        final long now = startTime / 1000;

        Session.Builder builder = new Session.Builder()
                .setName(sessionName)
                .setIdentifier(sessionId)
                .setStartTime(now, TimeUnit.SECONDS);
        if (sessionDescription != null) {
            builder.setDescription(sessionDescription);
        }
        if (fitnessActivity != null) {
            builder.setDescription(fitnessActivity);
        }
        mSession = builder.build();

        Log.d(TAG, "Starting session");
        Fitness.SessionsApi.startSession(mGoogleApiClient, mSession)
                .setResultCallback(new ResultCallbacks<Status>() {
                    @Override
                    public void onSuccess(@NonNull Status status) {
                        Log.d(TAG, "Successfully started session " + mSession.getIdentifier());
                    }

                    @Override
                    public void onFailure(@NonNull Status status) {
                        Log.e(TAG, "Failed to start " + mSession.getIdentifier());
                    }
                });
    }

    /**
     * Stop ongoing session. It's safe to call this method even if no session is ongoing.
     */
    public void stopSession() {
        if (mSession == null || !mSession.isOngoing()) {
            Log.w(TAG, "No active session to stop");
            return;
        }

        Log.d(TAG, "Stopping session");
        Fitness.SessionsApi.stopSession(mGoogleApiClient, mSession.getIdentifier())
                .setResultCallback(new ResultCallbacks<SessionStopResult>() {
                    @Override
                    public void onSuccess(@NonNull SessionStopResult sessionStopResult) {
                        Log.d(TAG, "Successfully stopped session " + mSession.getIdentifier());
                    }

                    @Override
                    public void onFailure(@NonNull Status status) {
                        Log.e(TAG, "Failed to stop " + mSession.getIdentifier());
                    }
                });

        cancelSubscription();
    }
}
