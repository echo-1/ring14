package com.example.terminator.ring14;

public class Constants {

    /* Server constants -- begin */

    public static final String STATUS_PATH = "/status";

    public static final String SERVER_READY_RESPONSE_STRING = "1";

    public static final String SERVER_NOT_READY_RESPONSE_STRING = "0";

    public static final String GET_TOKEN_PATH = "/get_token";

    public static final String PAYMENT_NONCE_PATH = "/payment_nonce";

    public static final String KEY_PAYMENT_NONCE = "payment_nonce";

    public static final String KEY_PAYMENT_AMOUNT = "payment_amount";

    public static final String KEY_FIREBASE_UID = "firebase_uid";

    public static final String FIREBASE_TABLE_USER_ROOT = "/users/";
    public static final String FIREBASE_TABLE_DONATION_ROOT = "/transactions/";
    public static final String FIREBASE_TABLE_DONATION_AMOUNT = "amount";
    public static final String FIREBASE_TABLE_DONATION_DISTANCE = "distance";
    public static final String FIREBASE_TABLE_DONATION_TIMESTAMP = "timestamp";
    public static final String FIREBASE_TABLE_DONATION_UID = "uid";
    public static final String FIREBASE_TABLE_DONATION_TRANSACTION_ID = "transactionId";

    public static final String FIREBASE_TABLE_TOTAL_ROOT = "/total";
    public static final String FIREBASE_TABLE_CURRENT_GOAL = "goal";
    public static final String FIREBASE_TABLE_CURRENT_DISTANCE = "current_distance";

    /* In the future we might want to do multiple donation sessions, get ready for it */
    /* TODO: Drop this constant and add proper logic */
    public static final String DONATION_SESSION = "0";

    public final static double EUR_TO_KM = 0.2;

    /* Server constants -- end */


    /* App constants -- begin */

    public final static boolean LOCAL_TEST = true;

    public final static String SERVER_URL = "http://example.com/";

    public final static String SERVER_GET_STATUS_URL = SERVER_URL + STATUS_PATH;

    public final static String SERVER_GET_CLIENT_TOKEN_URL = SERVER_URL + GET_TOKEN_PATH;

    public final static String SERVER_POST_NONCE_URL = SERVER_URL + PAYMENT_NONCE_PATH;

    /* App constants -- end */

}
