package com.example.terminator.ring14.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.terminator.ring14.R;
import com.example.terminator.ring14.model.FitnessData;
import com.example.terminator.ring14.services.FitnessService;
import com.example.terminator.ring14.utils.PermissionsUtils;
import com.example.terminator.ring14.utils.StringGenerator;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends BaseClientActivity implements OnMapReadyCallback,
        ResultCallback<LocationSettingsResult> {

    public static final String TAG = MapsActivity.class.getSimpleName();

    public static final int FITNESS_DATA_CHANGED = 3;

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private static final int REQUEST_CHECK_SETTINGS = 2;

    private GoogleMap mMap;

    private Polyline mTrack;
    private BroadcastReceiver mBroadcastReceiver;

    private boolean mSetup = false;

    FitnessService mService;
    boolean mBound = false;

    private double mGoalDistance = 0;

    private String mDonationId = null;

    private boolean mIsOrientationChange = false;

    private boolean mIsListenerRegistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        View loadingAnimation = findViewById(R.id.loading_animation);
        loadingAnimation.setBackgroundColor(Color.WHITE);

        if (savedInstanceState != null) {
            mIsOrientationChange = true;
        } else {
            /* Let's wait for ACTION_SERVICE_READY */
            showLoadingAnimation(true);
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mGoalDistance = extras.getDouble(FitnessService.KEY_GOAL_DISTANCE);
            mDonationId = extras.getString(FitnessService.KEY_DONATION_ID);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(FitnessService.ACTION_UPDATE_LOCATION)) {
                    float lat = intent.getFloatExtra(FitnessService.INTENT_EXTRA_LATITUDE, 0);
                    float lng = intent.getFloatExtra(FitnessService.INTENT_EXTRA_LONGITUDE, 0);
                    extendTrack(new LatLng(lat, lng));
                } else if (intent.getAction().equals(FitnessService.ACTION_UPDATE_DISTANCE)) {
                    updateDistanceText(intent.getFloatExtra(FitnessService.INTENT_EXTRA_DISTANCE, 0));
                } else if (intent.getAction().equals(FitnessService.ACTION_UPDATE_SPEED)) {
                    updateSpeedText(intent.getFloatExtra(FitnessService.INTENT_EXTRA_SPEED, 0));
                } else if (intent.getAction().equals(FitnessService.ACTION_START) ||
                        intent.getAction().equals(FitnessService.ACTION_STOP)) {
                    updateButtonText();
                } else if (intent.getAction().equals(FitnessService.ACTION_SERVICE_READY)) {
                    bulkUpdateFromService();
                    updateButtonText();
                    showLoadingAnimation(false);
                } else if (intent.getAction().equals(FitnessService.ACTION_TIME_TICK)) {
                    long millis = intent.getLongExtra(FitnessService.INTENT_EXTRA_TIME_TICK, 0);
                    updateTimerText(millis);
                }
            }
        };

        PermissionsUtils.checkAndRequestLocationPermission(this, REQUEST_LOCATION_PERMISSION);

        Button fitnessServiceButton = (Button) findViewById(R.id.fitness_service_button_start);
        fitnessServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mService.getSessionExists()) {
                    fitnessSessionDeleteDialog();
                } else if (!mService.isActive() && !mService.isActivating()) {
                    // TODO: set correct activity
                    mService.startSession(FitnessActivities.RUNNING);
                    resetInfo();
                    updateButtonText();
                } else {
                    mService.stopSession();
                    setResult(FITNESS_DATA_CHANGED);
                    mService.setSessionExists(true);
                    updateButtonText();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, FitnessService.class);

        /*
         * If mDonationId is null, then we are restarting the activity after it's
         * been killed. If this is the case, don't pass extra data to the service so that
         * it retrieves the necessary data from SharedPreferences.
         */
        if (mDonationId != null) {
            intent.putExtra(FitnessService.KEY_DONATION_ID, mDonationId);
            intent.putExtra(FitnessService.KEY_GOAL_DISTANCE, mGoalDistance);
        } else {
            SharedPreferences sharedPref =
                    getSharedPreferences(FitnessService.PREF_NAME, MODE_PRIVATE);
            boolean active = sharedPref.getBoolean(FitnessService.KEY_ACTIVE, false);
            /*
             * If active is true, then we are resuming an ongoing activity. We could query
             * fit to know this, but we are already storing this info locally.
             */
            intent.putExtra(FitnessService.INTENT_READ_SHARED_PREF, active);
        }
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            if (!mService.isActive() && !mService.isActivating()) {
                Intent intent = new Intent(this, FitnessService.class);
                stopService(intent);
            }
            unbindService(mConnection);
            mBound = false;
        }
    }

    private void showLoadingAnimation(boolean show) {
        View loadingAnimation = findViewById(R.id.loading_animation);
        loadingAnimation.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void fitnessSessionDeleteDialog() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.donation_replace_fitness_session_title))
                .setMessage(getString(R.string.donation_replace_fitness_session_message))
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new FitnessData(getApiClient()).deleteSession(mDonationId);
                                mService.setSessionExists(false);
                                setResult(FITNESS_DATA_CHANGED);
                                updateButtonText();
                                resetInfo();
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void updateTimerText(long millis) {
        TextView timer = (TextView) findViewById(R.id.fitness_activity_timer);
        timer.setText(StringGenerator.millisToTime(getApplicationContext(), millis));
    }

    private void updateButtonText() {
        Button fitnessServiceButton = (Button) findViewById(R.id.fitness_service_button_start);
        int idString;
        if (mService.isActive()) {
            idString = R.string.service_stop;
        } else if (mService.isActivating()) {
            idString = R.string.service_activating;
        } else if (mService.getSessionExists()) {
            idString = R.string.service_delete;
        } else {
            idString = R.string.service_start;
        }
        fitnessServiceButton.setText(getString(idString));
    }

    private void bulkUpdateFromService() {
        mTrack.setPoints(mService.getPoints());
        updateDistanceText(mService.getCumulativeDistance());
        updateSpeedText(mService.getCurrentSpeed());
        mGoalDistance = mService.getGoalDistance();
        mDonationId = mService.getDonationId();
        if (mService.getSessionExists()) {
            updateTimerText(mService.getSessionDuration());
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            FitnessService.LocalBinder binder = (FitnessService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;

            if (!mIsListenerRegistered) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(FitnessService.ACTION_UPDATE_LOCATION);
                intentFilter.addAction(FitnessService.ACTION_UPDATE_DISTANCE);
                intentFilter.addAction(FitnessService.ACTION_UPDATE_SPEED);
                intentFilter.addAction(FitnessService.ACTION_START);
                intentFilter.addAction(FitnessService.ACTION_STOP);
                intentFilter.addAction(FitnessService.ACTION_SERVICE_READY);
                intentFilter.addAction(FitnessService.ACTION_TIME_TICK);
                LocalBroadcastManager.getInstance(MapsActivity.this)
                        .registerReceiver(mBroadcastReceiver, intentFilter);
                mIsListenerRegistered = true;
            }

            /* In case we miss ACTION_SERVICE_READY */
            if (mService.isReady()) {
                bulkUpdateFromService();
                updateButtonText();
                showLoadingAnimation(false);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mIsListenerRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
            mIsListenerRegistered = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        setupMap();
    }

    @Override
    public void onBackPressed() {
        /* Don't close the activity if the fitness session is active */
        if (mService.isActive()) {
            moveTaskToBack(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setupMap();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission
                        .ACCESS_FINE_LOCATION)) {
                    PermissionsUtils.requestLocationPermissionOrExitDialog(this,
                            REQUEST_LOCATION_PERMISSION);
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mTrack = mMap.addPolyline(new PolylineOptions()
                .color(Color.BLUE)
                .width(5));

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (mTrack.getPoints().size() > 0) {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (LatLng latLng : mTrack.getPoints()) {
                        builder.include(latLng);
                    }
                    LatLngBounds bounds = builder.build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 15);
                    mMap.animateCamera(cameraUpdate);
                }
            }
        });
    }

    private void extendTrack(LatLng newPoint) {
        List<LatLng> points = mTrack.getPoints();
        points.add(newPoint);
        mTrack.setPoints(points);
    }

    private void resetInfo() {
        TextView textView;
        textView = (TextView) findViewById(R.id.fitness_activity_distance);
        textView.setText(getText(R.string.map_missing_value));
        textView = (TextView) findViewById(R.id.fitness_activity_speed);
        textView.setText(getText(R.string.map_missing_value));
        mTrack.setPoints(new ArrayList<LatLng>());
        updateTimerText(0);
    }

    private void updateSpeedText(float speed) {
        TextView textView = (TextView) findViewById(R.id.fitness_activity_speed);
        textView.setText(StringGenerator.speedToText(getApplicationContext(), speed));
    }

    private void updateDistanceText(float distance) {
        TextView textView = (TextView) findViewById(R.id.fitness_activity_distance);
        textView.setText(StringGenerator.sessionDistanceToText(getApplicationContext(), distance));
    }

    private void locateOnMap() {
        if (PermissionsUtils.checkLocationPermission(this)) {
            mMap.setMyLocationEnabled(true);
        }
    }

    private void checkLocationSettings() {
        LocationRequest locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .setAlwaysShow(true)
                .build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        getApiClient(),
                        locationSettingsRequest
                );
        result.setResultCallback(this);
    }

    private void setInitialPosition() {
        if (PermissionsUtils.checkLocationPermission(this)) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(getApiClient());
            if (location != null) {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(location
                        .getLatitude(), location.getLongitude()), 15);
                mMap.moveCamera(cameraUpdate);
            }
        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.d(TAG, "All location settings are satisfied.");
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.d(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(MapsActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.d(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.d(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    private void setupMap() {
        /* We need GoogleApiClient, so setup the map from onSignInSucceeded() */
        beginUserInitiatedSignIn();
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {
        if (PermissionsUtils.checkLocationPermission(this) && !mSetup) {
            if (!mIsOrientationChange) {
                checkLocationSettings();
                setInitialPosition();
            }
            locateOnMap();
            mSetup = true;
        }
    }
}
