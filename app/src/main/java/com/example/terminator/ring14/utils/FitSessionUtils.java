package com.example.terminator.ring14.utils;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class FitSessionUtils {

    /**
     * Extract cumulative distance from the given list of DataSet.
     *
     * @param dataSets DataSet with the distance deltas
     * @return cumulative distance
     */
    public static float getDistanceFromDataSets(List<DataSet> dataSets) {
        float distance = 0;
        for (DataSet dataSet : dataSets) {
            if (DataType.TYPE_DISTANCE_DELTA.equals(dataSet.getDataType())) {
                for (DataPoint dp : dataSet.getDataPoints()) {
                    distance += dp.getValue(Field.FIELD_DISTANCE).asFloat();
                }
            }
        }
        return distance;
    }

    /**
     * Extract location samples from the given list of DataSet.
     *
     * @param dataSets DataSet with the location samples
     * @return List of location samples found
     */
    public static List<LatLng> getLocationSamplesFromDataSets(List<DataSet> dataSets) {
        List<LatLng> latLngs = new ArrayList<>();
        for (DataSet dataSet : dataSets) {
            if (DataType.TYPE_LOCATION_SAMPLE.equals(dataSet.getDataType())) {
                for (DataPoint dp : dataSet.getDataPoints()) {
                    float lat = dp.getValue(Field.FIELD_LATITUDE).asFloat();
                    float lng = dp.getValue(Field.FIELD_LONGITUDE).asFloat();
                    latLngs.add(new LatLng(lat, lng));
                }
            }
        }
        return latLngs;
    }
}
