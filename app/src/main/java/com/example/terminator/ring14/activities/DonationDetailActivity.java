package com.example.terminator.ring14.activities;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.terminator.ring14.R;
import com.example.terminator.ring14.model.Donation;
import com.example.terminator.ring14.model.FitnessClient;
import com.example.terminator.ring14.model.FitnessData;
import com.example.terminator.ring14.services.FitnessService;
import com.example.terminator.ring14.utils.BitmapUtils;
import com.example.terminator.ring14.utils.DonationUtils;
import com.example.terminator.ring14.utils.FitSessionUtils;
import com.example.terminator.ring14.utils.SocialShareUtils;
import com.example.terminator.ring14.utils.StringGenerator;
import com.example.terminator.ring14.utils.PermissionsUtils;
import com.example.terminator.ring14.helpers.ResizeHeightAnimation;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class DonationDetailActivity extends BaseActivity implements OnMapReadyCallback {

    private static final String TAG = DonationDetailActivity.class.getSimpleName();

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private static final int REQUEST_MAPS_ACTIVITY = 2;
    private static final int REQUEST_WRITE_EXT_STORAGE = 3;
    private static final int REQUEST_PICK_PHOTO = 4;

    public static final String EXTRA_DONATION_PARCELABLE = "donation";
    public static final String KEY_IMAGE_URI = "image_uri";
    private static final String KEY_SESSION_EXISTS = "session_exists";
    private static final String KEY_DONATION_DATA = "donation_data";
    private static final String KEY_TEMP_IMAGE_URI = "temp_image_uri";
    private static final String KEY_FIT_DATA_FETCHED = "fit_data_fetched";

    private Donation mDonation;

    private FitnessData mFitnessData;

    private boolean mNeedPostpermGrantedSetup = false;

    private boolean mSessionExists = false;

    private boolean mFitDataFetched = false;

    private ImageView mImageView;
    private FrameLayout mFrameLayout;

    private GoogleMap mMap;
    private List<LatLng> mTrackPoints;
    private CameraUpdate mCameraUpdate;
    private boolean mMapLoaded = false;

    private Uri mImageUriTemp;

    private View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), ImageFullscreenActivity.class);
            Uri imageUri = DonationUtils.getImageForDonation(DonationDetailActivity.this, mDonation);
            intent.putExtra(KEY_IMAGE_URI, imageUri.toString());
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_donation_detail);

        if (savedInstanceState != null) {
            mDonation = savedInstanceState.getParcelable(KEY_DONATION_DATA);
            mSessionExists = savedInstanceState.getBoolean(KEY_SESSION_EXISTS, false);
            mFitDataFetched = savedInstanceState.getBoolean(KEY_FIT_DATA_FETCHED, false);

            findViewById(R.id.fit_session_details)
                    .setVisibility(mSessionExists ? View.VISIBLE : View.GONE);

            configureFab();

            mImageUriTemp = savedInstanceState.getParcelable(KEY_TEMP_IMAGE_URI);
        } else {
            mDonation = getIntent().getParcelableExtra(EXTRA_DONATION_PARCELABLE);
            if (mDonation == null) {
                Log.e(TAG, "Missing parcelable");
                finish();
                return;
            }
            showLoadingAnimation(true);
        }

        Uri imageUri = DonationUtils.getImageForDonation(this, mDonation);
        if (imageUri != null) {
            showThumbnail(imageUri);
        }

        TextView tvIdTrans = (TextView) findViewById(R.id.tv_transaction_id);
        TextView tvDate = (TextView) findViewById(R.id.tv_donation_date);
        TextView tvAmount = (TextView) findViewById(R.id.tv_donated_amount);
        TextView tvPurchased = (TextView) findViewById(R.id.tv_purchased_km);

        tvIdTrans.setText(mDonation.getTransactionId());
        String date = StringGenerator.timestampToShortDateTimeText(this, mDonation.getTimestamp());
        tvDate.setText(date);
        tvAmount.setText(StringGenerator.moneyToText(this, mDonation.getAmount()));
        tvPurchased.setText(StringGenerator.distanceToText(this, mDonation.getDistance()));

        PermissionsUtils.checkAndRequestLocationPermission(this, REQUEST_LOCATION_PERMISSION);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_preview);
        mapFragment.getMapAsync(this);
        mapFragment.setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        /* Make sure permissions weren't revoked while paused */
        if (mFitnessData != null) {
            PermissionsUtils.checkAndRequestLocationPermission(this, REQUEST_LOCATION_PERMISSION);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.donation_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_take_pic:
                checkPermission();
                return true;
            case R.id.action_share:
                try {
                    Uri imageUri = DonationUtils.getImageForDonation(this, mDonation);
                    if (imageUri == null) {
                        imageUri = DonationUtils.getDefaultImage(this);
                    }
                    share(imageUri);
                } catch (IOException e) {
                    Toast.makeText(this, R.string.toast_share_error, Toast.LENGTH_SHORT).show();
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mNeedPostpermGrantedSetup) {
                        mNeedPostpermGrantedSetup = false;
                        setupFitnessClient();
                    }
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission
                                    .ACCESS_FINE_LOCATION)) {
                        PermissionsUtils.requestLocationPermissionOrExitDialog(this,
                                REQUEST_LOCATION_PERMISSION);
                    }
                }
                break;

            case REQUEST_WRITE_EXT_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        PermissionsUtils.requestStoragePermission(this, REQUEST_WRITE_EXT_STORAGE);
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_MAPS_ACTIVITY) {
            if (resultCode == MapsActivity.FITNESS_DATA_CHANGED) {
                hideFitDetailsCard();
                showLoadingAnimation(true);
                fetchFitnessSession();
            }
        }
        if (requestCode == REQUEST_PICK_PHOTO && resultCode == Activity.RESULT_OK) {
            Uri imageUri;
            if (data == null || data.getData() == null) {
                imageUri = mImageUriTemp;
                galleryAddPic(imageUri);
                mImageUriTemp = null;
            } else {
                imageUri = data.getData();
            }
            DonationUtils.addImageToDonation(this, mDonation, imageUri);
            showThumbnail(imageUri);
        }
    }

    private void galleryAddPic(Uri imageUri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(imageUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void setupFitnessClient() {
        FitnessClient fitnessClient = new FitnessClient(this, new FitnessClient.Connection() {
            @Override
            public void onConnected() {
                fetchFitnessSession();
            }
        });
        mFitnessData = new FitnessData(fitnessClient);
        fitnessClient.connect();
    }

    @Override
    protected void onFirebaseAuthenticated() {
        if (PermissionsUtils.checkLocationPermission(this)) {
            if (!mFitDataFetched) {
                setupFitnessClient();
            }
        } else {
            /* We are still waiting for the permissions, do the setup when we get them */
            mNeedPostpermGrantedSetup = true;
        }
    }

    @Override
    protected void onFirebaseAuthenticationFailure() {
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_FIT_DATA_FETCHED, mFitDataFetched);
        savedInstanceState.putBoolean(KEY_SESSION_EXISTS, mSessionExists);
        savedInstanceState.putParcelable(KEY_DONATION_DATA, mDonation);

        /* Allow to recover the URI in case the activity is destroyed while taking the picture */
        if (mImageUriTemp != null) {
            savedInstanceState.putString(KEY_TEMP_IMAGE_URI, mImageUriTemp.toString());
        }

        super.onSaveInstanceState(savedInstanceState);
    }

    private void fetchFitnessSession() {
        mFitnessData.getSession(mDonation.getId(), new FitnessData.ReadCallback() {
            @Override
            public void onDataReady(Session session, List<DataSet> dataSets) {
                updateSessionData(session, dataSets);
                mSessionExists = true;
                mFitDataFetched = true;
                configureFab();
                showLoadingAnimation(false);
            }

            @Override
            public void onDataNotFound() {
                mSessionExists = false;
                mFitDataFetched = true;
                configureFab();
                showLoadingAnimation(false);
            }
        });
    }

    private void startFitnessActivity() {
        Intent intent = new Intent(DonationDetailActivity.this, MapsActivity.class);
        intent.putExtra(FitnessService.KEY_DONATION_ID, mDonation.getId());
        intent.putExtra(FitnessService.KEY_GOAL_DISTANCE, mDonation.getDistance());
        startActivityForResult(intent, REQUEST_MAPS_ACTIVITY);
    }

    private void configureFab() {
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFitnessActivity();
            }
        });
        fab.show();
    }

    private void showLoadingAnimation(boolean show) {
        findViewById(R.id.loading_animation).setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void checkPermission() {
        if (!PermissionsUtils.checkStoragePermission(this)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_EXT_STORAGE);
        } else {
            selectImage();
        }
    }

    private void selectImage() {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        } else {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        }
        intent.setType("image/*");

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String fileName = "IMG_" + formatter.format(new Date()) + ".jpg";
        File outFile = new File(DonationUtils.getAlbumStorageDir(), fileName);
        mImageUriTemp = Uri.fromFile(outFile);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUriTemp);

        String pickTitle = "Select or take a new Picture";
        Intent chooserIntent = Intent.createChooser(intent, pickTitle);
        chooserIntent.putExtra
                (
                        Intent.EXTRA_INITIAL_INTENTS,
                        new Intent[] { takePictureIntent }
                );
        startActivityForResult(chooserIntent, REQUEST_PICK_PHOTO);
    }

    private void showThumbnail(Uri imageUri) {
        if (mFrameLayout == null) {
            initImageThumbnailView();
        }

        try {
            /*
             * Use the size of the screen since we might have no view to measure.
             * We only need an approximate value for the preview, so take the smallest screen
             * dimension and use that to specify both the width and the height.
             */
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int size = Math.min(metrics.widthPixels, metrics.heightPixels);
            Bitmap bitmap = BitmapUtils.decodeSampledBitmap(this, imageUri, size, size);
            mFrameLayout.setVisibility(View.VISIBLE);
            mImageView.setImageBitmap(bitmap);

            /* If we removed the image, the height of the view is 0 */
            mFrameLayout.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            mFrameLayout.requestLayout();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Could not load " + imageUri, e);
            DonationUtils.removeImageForDonation(this, mDonation);
            hideThumbnail();
            Toast.makeText(this, R.string.toast_image_not_found, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Log.e(TAG, "Could not load " + imageUri, e);
            Toast.makeText(this, R.string.toast_image_load_error, Toast.LENGTH_SHORT).show();
            hideThumbnail();
        }
    }

    private void initImageThumbnailView() {
        LayoutInflater inflater = getLayoutInflater();
        View thumbnailView = inflater.inflate(R.layout.image_thumbnail,
                (ViewGroup) findViewById(R.id.cardview_image_thumbnail_placeholder), false);
        mFrameLayout = (FrameLayout) findViewById(R.id.cardview_image_thumbnail_placeholder);
        mImageView = (ImageView) thumbnailView.findViewById(R.id.user_image_share);
        TextView tvRemoveImage = (TextView) thumbnailView.findViewById(R.id.remove_image);
        tvRemoveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DonationUtils.removeImageForDonation(DonationDetailActivity.this, mDonation);
                hideThumbnail();
            }
        });
        mFrameLayout.addView(thumbnailView);
        mImageView.setOnClickListener(imageClickListener);
    }

    private void hideThumbnail() {
        if (mFrameLayout != null && mFrameLayout.getVisibility() == View.VISIBLE) {
            mFrameLayout.animate().translationX(mFrameLayout.getWidth())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            ResizeHeightAnimation resizeHeightAnimation =
                                    new ResizeHeightAnimation(mFrameLayout, 0);
                            resizeHeightAnimation.setDuration(200);
                            resizeHeightAnimation.setAnimationListener(
                                    new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    mFrameLayout.setVisibility(View.GONE);
                                    mFrameLayout.setTranslationX(0);
                                    mImageView.setImageDrawable(null);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {
                                }
                            });
                            mFrameLayout.startAnimation(resizeHeightAnimation);
                        }
                    });
        }
    }

    private void share(Uri imageUri) {
        String message = StringGenerator.getDonationShareMessage(this, mDonation);
        SocialShareUtils.shareData(this, imageUri, message);
    }

    private void updateSessionData(Session session, List<DataSet> dataSets) {
        long durationSeconds = session.getEndTime(TimeUnit.SECONDS) -
                session.getStartTime(TimeUnit.SECONDS);
        TextView tvTotalTime = (TextView) findViewById(R.id.tv_total_time_value);
        tvTotalTime.setText(DateUtils.formatElapsedTime(durationSeconds));

        TextView tvDistance = (TextView) findViewById(R.id.tv_distance_value);
        float distance = FitSessionUtils.getDistanceFromDataSets(dataSets);
        String distanceString = StringGenerator.distanceToText(getApplicationContext(), distance);
        tvDistance.setText(distanceString);

        TextView tvPace = (TextView) findViewById(R.id.tv_average_pace_value);
        if (distance > 0) {
            long pace = Math.round(durationSeconds / distance);
            tvPace.setText(DateUtils.formatElapsedTime(pace));
        } else {
            tvPace.setText("-");
        }

        findViewById(R.id.fit_session_details).setVisibility(View.VISIBLE);

        mTrackPoints = FitSessionUtils.getLocationSamplesFromDataSets(dataSets);
        updateTrack();
    }

    private void hideFitDetailsCard() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        findViewById(R.id.fit_session_details).setVisibility(View.GONE);
        fab.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMapLoaded = true;
                updateTrack();
            }
        });
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                startFitnessActivity();
            }
        });
        updateTrack();
    }

    private void updateTrack() {
        if (mTrackPoints == null || mMap == null) {
            /* Map or data not ready */
            return;
        }

        if (mMapLoaded && mCameraUpdate != null) {
            /* The track is already drawn, but the camera position is most likely wrong */
            mMap.moveCamera(mCameraUpdate);
        }

        if (mTrackPoints.size() == 0) {
            /* We have no points to draw the track */
            return;
        }

        Polyline track = mMap.addPolyline(new PolylineOptions()
                .color(Color.BLUE)
                .width(5));
        track.setPoints(mTrackPoints);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : mTrackPoints) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 15);
        mMap.moveCamera(mCameraUpdate);
    }
}
