package com.example.terminator.ring14.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.example.terminator.ring14.Constants;
import com.example.terminator.ring14.R;
import com.example.terminator.ring14.helpers.FirebaseConnection;
import com.example.terminator.ring14.model.Donation;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class DonationUtils {

    private static final String IMAGE_EXTENSION = ".png"; // Format used by BitmapUtils
    private static final String DEFAULT_IMAGE_NAME = "ring14" + IMAGE_EXTENSION;

    private static final String DONATIONS_IMAGES_ALBUM_NAME = "Ring14";
    private static final String IMAGES_SHARED_PREF = "images_pref";

    public interface SingleValueCallback {
        void onDataChange(Donation donation);
        void onCancelled();
    }

    public interface MultiValueCallback {
        void onDataChange(List<Donation> donations);
        void onCancelled();
    }

    public static final String TEST_DONATION_ID = UUID.randomUUID().toString();

    private static final String TAG = DonationUtils.class.getSimpleName();

    /**
     * Get from the Firebase database the donation with the given id.
     * Incomplete Donation objects might be returned, use Donation.isValid()
     */
    public static void getDonation(FirebaseConnection firebaseConnection, String donationId,
                                   final SingleValueCallback callback) {
        if (TEST_DONATION_ID.equals(donationId)) {
            callback.onDataChange(getTestDonation());
            return;
        }

        String child = Constants.FIREBASE_TABLE_DONATION_ROOT + donationId;
        firebaseConnection.readDB(child, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Donation donation = parseFirebaseDonationData(dataSnapshot);
                callback.onDataChange(donation);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Could not get donation: " + databaseError);
                callback.onCancelled();
            }
        });
    }

    public static Donation getTestDonation() {
        int distance = 25;
        double amount = distance / Constants.EUR_TO_KM; // Ignore user preference
        Donation donation = new Donation(TEST_DONATION_ID, "transaction_id", amount, distance,
                System.currentTimeMillis());
        return donation;
    }

    /**
     * Get from the Firebase database the donations of the given user.
     * Incomplete Donation objects might be returned, use Donation.isValid()
     */
    public static void getUserDonations(FirebaseConnection firebaseConnection, String userId,
                                            final MultiValueCallback callback) {
        String child = Constants.FIREBASE_TABLE_USER_ROOT + userId +
                Constants.FIREBASE_TABLE_DONATION_ROOT;
        firebaseConnection.readDB(child, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Donation> donations = new ArrayList<>();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Donation donation = parseFirebaseDonationData(data);
                    donations.add(donation);
                }
                callback.onDataChange(donations);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Could not get donation: " + databaseError);
                callback.onCancelled();
            }
        });
    }

    /**
     * Extract Donation from the given com.google.firebase.database.DataSnapshot
     * Incomplete Donation objects might be returned, use Donation.isValid()
     */
    public static Donation parseFirebaseDonationData(DataSnapshot dataSnapshot) {
        Donation donation = dataSnapshot.getValue(Donation.class);
        donation.setId(dataSnapshot.getKey());

        if (!donation.isValid()) {
            Log.d(TAG, "Invalid donation found");
        }

        return donation;
    }

    public static Comparator<Donation> getDistanceComparator() {
        class DonationDistanceComparator implements Comparator<Donation> {
            public int compare(Donation donation1, Donation donation2) {
                return Double.compare(donation1.getDistance(), donation2.getDistance());
            }
        }
        return new DonationDistanceComparator();
    }

    public static Comparator<Donation> getAmountComparator() {
        class DonationAmountComparator implements Comparator<Donation> {
            public int compare(Donation donation1, Donation donation2) {
                return Double.compare(donation1.getAmount(), donation2.getAmount());
            }
        }
        return new DonationAmountComparator();
    }

    public static Comparator<Donation> getDateComparator() {
        class DonationDateComparator implements Comparator<Donation> {
            public int compare(Donation donation1, Donation donation2) {
                return Double.compare(donation1.getTimestamp(), donation2.getTimestamp());
            }
        }
        return new DonationDateComparator();
    }

    public static double distanceToEur(Context context, double distance) {
        String metric = context.getString(R.string.metric_system_value);
        String pref = StringGenerator.getPreferredUnit(context);
        if (pref.equals(metric)) {
            return distance / Constants.EUR_TO_KM;
        } else {
            return distance * 0.621371 / Constants.EUR_TO_KM;
        }
    }

    public static double eurToDistance(Context context, double eur) {
        String metric = context.getString(R.string.metric_system_value);
        String pref = StringGenerator.getPreferredUnit(context);
        if (pref.equals(metric)) {
            return eur * Constants.EUR_TO_KM;
        } else {
            return eur * (0.621371 * Constants.EUR_TO_KM);
        }
    }

    public static File saveDefaultImage(Context context) throws IOException {
        Log.d(TAG, "Saving default image");
        File filepath = new File(context.getFilesDir(), DEFAULT_IMAGE_NAME);
        Bitmap pic = BitmapFactory.decodeResource(context.getResources(), R.drawable.ring_logo);
        return BitmapUtils.saveImage(filepath, pic);
    }

    public static Uri getDefaultImage(Context context) throws IOException {
        File defaultImage = new File(context.getFilesDir(), DEFAULT_IMAGE_NAME);
        if (!defaultImage.exists()) {
            defaultImage = saveDefaultImage(context);
        }
        Uri photoURI = FileProvider.getUriForFile(context,
                context.getResources().getString(R.string.file_provider_authority),
                defaultImage);
        return photoURI;
    }

    public static void addImageToDonation(Context context, Donation donation, Uri imageUri) {
        SharedPreferences sharedPref = context.getSharedPreferences(IMAGES_SHARED_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPref.edit();
        edit.putString(donation.getId(), imageUri.toString());
        edit.apply();
    }

    public static void removeImageForDonation(Context context, Donation donation) {
        SharedPreferences sharedPref = context.getSharedPreferences(IMAGES_SHARED_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(donation.getId());
        editor.apply();
    }

    public static Uri getImageForDonation(Context context, Donation donation) {
        SharedPreferences sharedPref = context.getSharedPreferences(IMAGES_SHARED_PREF,
                Context.MODE_PRIVATE);
        if (sharedPref.contains(donation.getId())) {
            return Uri.parse(sharedPref.getString(donation.getId(), ""));
        } else {
            return null;
        }
    }

    public static File getAlbumStorageDir() {
        // Get the directory for the app's private pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), DONATIONS_IMAGES_ALBUM_NAME);
        if (!file.mkdirs()) {
            Log.e(TAG, "Directory not created");
        }
        return file;
    }
}
