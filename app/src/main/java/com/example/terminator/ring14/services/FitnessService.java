package com.example.terminator.ring14.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.terminator.ring14.R;
import com.example.terminator.ring14.activities.MapsActivity;
import com.example.terminator.ring14.model.FitnessClient;
import com.example.terminator.ring14.model.FitnessData;
import com.example.terminator.ring14.model.FitnessRecording;
import com.example.terminator.ring14.model.FitnessSensors;
import com.example.terminator.ring14.utils.FitSessionUtils;
import com.example.terminator.ring14.utils.PermissionsUtils;
import com.example.terminator.ring14.utils.CustomTimer;
import com.example.terminator.ring14.utils.StringGenerator;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FitnessService extends Service implements LocationListener {

    public static final String TAG = FitnessService.class.getSimpleName();

    private static final int INTERVAL_MS = 5000;

    private static final int notifyID = 1;

    private static final String WAKELOCK_NAME = "FitnessService-wakelock";

    public static final String ACTION_START = "START";
    public static final String ACTION_STOP = "STOP";
    public static final String ACTION_UPDATE_LOCATION = "UPDATE_LOCATION";
    public static final String ACTION_UPDATE_DISTANCE = "UPDATE_DISTANCE";
    public static final String ACTION_UPDATE_SPEED = "UPDATE_SPEED";
    public static final String ACTION_SERVICE_READY = "SERVICE_READY";
    public static final String ACTION_TIME_TICK = "ACTION_TIME_TICK";

    public static final String INTENT_EXTRA_LATITUDE = "latitude";
    public static final String INTENT_EXTRA_LONGITUDE = "longitude";
    public static final String INTENT_EXTRA_DISTANCE = "distance";
    public static final String INTENT_EXTRA_SPEED = "speed";
    public static final String INTENT_READ_SHARED_PREF = "read_shared_pref";
    public static final String INTENT_EXTRA_TIME_TICK = "time_tick_extra";

    public static final String PREF_NAME = "service_status";
    public static final String KEY_GOAL_DISTANCE = "goal_distance";
    public static final String KEY_DONATION_ID = "donation_id";
    public static final String KEY_START_TIME = "timestamp";
    public static final String KEY_ACTIVE = "active";
    public static final String KEY_FITNESS_ACTIVITY = "fitness_activity";

    private FitnessClient mClient;

    private FitnessSensors mSensors;

    private FitnessRecording mRecording;

    private LocalBroadcastManager mBroadcastManager;

    private LocationRequest mLocationRequest;

    private List<LatLng> mPoints = new ArrayList<>();

    private NotificationCompat.Builder mNotificationBuilder;
    private NotificationManager mNotificationManager;

    private final CustomTimer mCustomTimer;

    private PowerManager.WakeLock mWakeLock;

    private long mStartTimeRelative;
    private long mSessionDuration = -1;
    private float mCumulativeDistance;
    private float mCurrentSpeed;

    private boolean mIsActive = false;
    private boolean mIsActivating = false;

    private String mFitnessActivity;

    private double mGoalDistance = 0;

    private String mDonationId;

    private final IBinder mBinder = new LocalBinder();

    private boolean mStarted = false;

    private SharedPreferences mSharedPreferences;

    private boolean mSessionExists = false;

    private boolean mIsReady = false;

    public FitnessService() {
        mCustomTimer = new CustomTimer(1000) {
            @Override
            public void onTick(long millisElapsed) {
                Intent intent = new Intent(ACTION_TIME_TICK);
                intent.putExtra(INTENT_EXTRA_TIME_TICK, millisElapsed);
                mBroadcastManager.sendBroadcast(intent);

                String notificationString = StringGenerator.getNotificationString(
                        getApplicationContext(), millisElapsed, mCumulativeDistance);
                mNotificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(notificationString));
                mNotificationManager.notify(notifyID, mNotificationBuilder.build());
            }
        };
    }

    public class LocalBinder extends Binder {
        public FitnessService getService() {
            return FitnessService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Bitmap logo = BitmapFactory.decodeResource(getResources(),
                R.drawable.ring_logo_notification_large);
        mNotificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ring_logo_notification_small)
                .setLargeIcon(logo)
                .setContentTitle(getString(R.string.app_name))
                .setShowWhen(false)
                .setOngoing(true);

        mSharedPreferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSession();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mStarted) {
            mBroadcastManager.sendBroadcast(new Intent(ACTION_SERVICE_READY));
            /* This service has already been set up, no need to do it again */
            return START_STICKY;
        }

        if (intent == null || intent.getBooleanExtra(INTENT_READ_SHARED_PREF, false)) {
            /* The service is being restarted, fetch the data from SharedPreferences */
            mIsReady = true;
            mBroadcastManager.sendBroadcast(new Intent(ACTION_SERVICE_READY));
            restoreStartedSessionStatus();
        } else {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                mGoalDistance = extras.getDouble(KEY_GOAL_DISTANCE);
                mDonationId = extras.getString(KEY_DONATION_ID);
            }
            loadSession();
        }

        mStarted = true;

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void prepareFitnessSession(boolean restart) {
        Log.d(TAG, "Prepare fitness session (restart=" + restart + ")");
        long sessionStartTime;
        if (restart) {
            sessionStartTime = getStartTime();
        } else {
            sessionStartTime = System.currentTimeMillis();
        }
        createFitnessClient(sessionStartTime);
        startLocationUpdates();
        buildNotification();

        mIsActive = true;
        mIsActivating = false;
        mCustomTimer.start(mStartTimeRelative);

        mBroadcastManager.sendBroadcast(new Intent().setAction(ACTION_START));

        if (!restart) {
            /* No need to save the status when resuming */
            saveStartedSessionStatus();
        }
    }

    public void createGoogleApiClient(FitnessClient.Connection connection) {
        if (mClient == null && PermissionsUtils.checkLocationPermission(this)) {
            Log.d(TAG, "Creating client");
            mClient = new FitnessClient(this, connection);
        } else {
            Log.d(TAG, "Updating client connected callback");
            /*
             * The client already exists, just update the callback. This can happen
             * when the service is started to load an old fitness session and we want to
             * overwrite it starting a new one.
             */
            mClient.setConnectionCallback(connection);
        }

        if (!mClient.getGoogleApiClient().isConnected() || !mClient.getGoogleApiClient()
                .isConnecting()) {
            Log.d(TAG, "Initiating client connection");
            mClient.connect();

            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_NAME);
        } else {
            /*
             * If the GoogleApiClient is already connected, the connection callback
             * is not going to be executed, so do it manually.
             * This should never happen, but who knows.
             */
            Log.d(TAG, "Client already connected, execute the callback");
            connection.onConnected();
        }
    }

    private void createFitnessClient(final long sessionStartTime) {
        mSensors = new FitnessSensors(mClient.getGoogleApiClient(), new FitnessSensors.DataPointListener() {
            @Override
            public void onDataPoint(DataPoint dataPoint) {
                Log.d(TAG, "Received data type: " + dataPoint.getDataType());

                Intent intent = new Intent();
                if (DataType.TYPE_LOCATION_SAMPLE.equals(dataPoint.getDataType())) {
                    intent.setAction(ACTION_UPDATE_LOCATION);
                    float lat = dataPoint.getValue(Field.FIELD_LATITUDE).asFloat();
                    intent.putExtra(INTENT_EXTRA_LATITUDE, lat);
                    float lng = dataPoint.getValue(Field.FIELD_LONGITUDE).asFloat();
                    intent.putExtra(INTENT_EXTRA_LONGITUDE, lng);
                    mPoints.add(new LatLng(lat, lng));
                } else if (DataType.TYPE_DISTANCE_DELTA.equals(dataPoint.getDataType())) {
                    /* Don't bother updating the notification */
                    mCumulativeDistance += dataPoint.getValue(Field.FIELD_DISTANCE).asFloat();
                    intent.setAction(ACTION_UPDATE_DISTANCE);
                    intent.putExtra(INTENT_EXTRA_DISTANCE, mCumulativeDistance);
                } else if (DataType.TYPE_SPEED.equals(dataPoint.getDataType())) {
                    mCurrentSpeed = dataPoint.getValue(Field.FIELD_SPEED).asFloat();
                    intent.setAction(ACTION_UPDATE_SPEED);
                    intent.putExtra(INTENT_EXTRA_SPEED, dataPoint.getValue(Field.FIELD_SPEED)
                            .asFloat());
                } else {
                    Log.w(TAG, "Unexpected data type: " + dataPoint.getDataType());
                    return;
                }
                mBroadcastManager.sendBroadcast(intent);
            }
        });

        mSensors.findFitnessDataSources();

        mRecording = new FitnessRecording(mClient.getGoogleApiClient());
        mRecording.startSession(mDonationId, getString(R.string.fit_session_name), null,
                mFitnessActivity, sessionStartTime);
    }

    private void buildNotification() {
        Intent resultIntent = new Intent(this, MapsActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationBuilder.setContentIntent(resultPendingIntent);
        startForeground(notifyID, mNotificationBuilder.build());
    }

    public void startSession(String activity) {
        Log.d(TAG, "Starting session");
        mStartTimeRelative = SystemClock.elapsedRealtime();
        mSessionDuration = -1;
        mCumulativeDistance = 0;
        mCurrentSpeed = 0;
        mFitnessActivity = activity;
        mPoints.clear();

        mIsActivating = true;
        createGoogleApiClient(new FitnessClient.Connection() {
            @Override
            public void onConnected() {
                prepareFitnessSession(false);
            }
        });

        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
    }

    public void stopSession() {
        Log.d(TAG, "Stopping session");
        resetActivePref();

        // If location permission was never granted, these are null
        if (mClient != null && mClient.getGoogleApiClient().isConnected()) {
            if (mSensors != null)
                mSensors.unregisterFitnessDataListener();
            if (mRecording != null)
                mRecording.stopSession();
            mClient.disconnect();
        }

        stopForeground(true);

        if (mIsActive) {
            mCustomTimer.stop();
            mSessionDuration = SystemClock.elapsedRealtime() - mStartTimeRelative;
            mBroadcastManager.sendBroadcast(new Intent().setAction(ACTION_STOP));
        }

        mIsActive = false;
        mIsActivating = false;

        if (mWakeLock != null && mWakeLock.isHeld())
            mWakeLock.release();
    }

    private void saveStartedSessionStatus() {
        Log.d(TAG, "Saving initial session status");
        SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
        sharedPreferencesEditor.putLong(KEY_GOAL_DISTANCE,
                Double.doubleToRawLongBits(mGoalDistance));
        sharedPreferencesEditor.putString(KEY_DONATION_ID, mDonationId);
        sharedPreferencesEditor.putLong(KEY_START_TIME, getStartTime());
        sharedPreferencesEditor.putBoolean(KEY_ACTIVE, mIsActive);
        sharedPreferencesEditor.putString(KEY_FITNESS_ACTIVITY, mFitnessActivity);
        sharedPreferencesEditor.apply();
    }

    private void restoreStartedSessionStatus() {
        Log.d(TAG, "Restore previous session");
        mGoalDistance = Double.longBitsToDouble(mSharedPreferences.getLong(KEY_GOAL_DISTANCE,
                Double.doubleToLongBits(0)));
        mDonationId = mSharedPreferences.getString(KEY_DONATION_ID, null);
        mIsActive = mSharedPreferences.getBoolean(KEY_ACTIVE, false);
        mStartTimeRelative = SystemClock.elapsedRealtime() -
                (System.currentTimeMillis() - mSharedPreferences.getLong(KEY_START_TIME, 0));
        mSessionDuration = -1;
        mFitnessActivity = mSharedPreferences.getString(KEY_FITNESS_ACTIVITY, null);

        mIsActivating = true;
        createGoogleApiClient(new FitnessClient.Connection() {
            @Override
            public void onConnected() {
                prepareFitnessSession(true);
            }
        });
    }

    private void resetActivePref() {
        Log.d(TAG, "Removing persistent initial session status");
        SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
        /* MainActivity reads KEY_ACTIVE to know if the service is running */
        sharedPreferencesEditor.putBoolean(KEY_ACTIVE, false);
        sharedPreferencesEditor.apply();
    }

    public boolean isReady() {
        return mIsReady;
    }

    public boolean isActive() {
        return mIsActive;
    }

    public boolean isActivating() {
        return mIsActivating;
    }

    public List<LatLng> getPoints() {
        return mPoints;
    }

    public long getStartTimeRelative() {
        return mStartTimeRelative;
    }

    public long getStartTime() {
        return System.currentTimeMillis() -
                (SystemClock.elapsedRealtime() - mStartTimeRelative);
    }

    public long getSessionDuration() {
        return mSessionDuration;
    }

    public float getCurrentSpeed() {
        return mCurrentSpeed;
    }

    public float getCumulativeDistance() {
        return mCumulativeDistance;
    }

    public double getGoalDistance() {
        return mGoalDistance;
    }

    public void setDonationId(String donationId) {
        if (mDonationId == null || !mDonationId.equals(donationId)) {
            mDonationId = donationId;
            mIsReady = false;
            loadSession();
        } else {
            /* Don't reload the same session */
            mBroadcastManager.sendBroadcast(new Intent(ACTION_SERVICE_READY));
        }
    }

    public String getDonationId() {
        return mDonationId;
    }

    public boolean getSessionExists() {
        return mSessionExists;
    }

    public void setSessionExists(boolean sessionExists) {
        mSessionExists = sessionExists;
    }

    private void loadSession() {
        Log.d(TAG, "Trying to load existing session");
        createGoogleApiClient(new FitnessClient.Connection() {

            @Override
            public void onConnected() {
                new FitnessData(mClient).getSession(mDonationId, new FitnessData.ReadCallback() {
                    @Override
                    public void onDataReady(Session session, List<DataSet> dataSets) {
                        Log.d(TAG, "Session loaded");
                        mSessionExists = true;
                        mCumulativeDistance = FitSessionUtils.getDistanceFromDataSets(dataSets);
                        mPoints = FitSessionUtils.getLocationSamplesFromDataSets(dataSets);
                        mClient.disconnect();
                        mSessionDuration = session.getEndTime(TimeUnit.MILLISECONDS) -
                                session.getStartTime(TimeUnit.MILLISECONDS);
                        mBroadcastManager.sendBroadcast(new Intent(ACTION_SERVICE_READY));
                        mIsReady = true;
                    }

                    @Override
                    public void onDataNotFound() {
                        Log.d(TAG, "No session found");
                        mSessionExists = false;
                        mBroadcastManager.sendBroadcast(new Intent(ACTION_SERVICE_READY));
                        mClient.disconnect();
                        mIsReady = true;
                    }
                });
            }
        });
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL_MS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {
        if (PermissionsUtils.checkLocationPermission(this)) {
            createLocationRequest();
            LocationServices.FusedLocationApi.requestLocationUpdates(mClient.getGoogleApiClient(),
                    mLocationRequest, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        /* Do nothing */
    }
}
