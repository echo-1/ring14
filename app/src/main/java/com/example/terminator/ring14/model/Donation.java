package com.example.terminator.ring14.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Donation implements Parcelable {

    private String mId;

    private String mTransactionId;

    private double mAmount;

    private double mDistance;

    private long mTimestamp = 0;

    public Donation() {
    }

    public Donation(double amount, double distance, long timestamp) {
        mAmount = amount;
        mDistance = distance;
        mTimestamp = timestamp;
    }

    public Donation(String transactionId, double amount, double distance, long timestamp) {
        this(amount, distance, timestamp);
        mTransactionId = transactionId;
    }

    public Donation(String id, String transactionId, double amount, double distance,
                    long timestamp) {
        this(transactionId, amount, distance, timestamp);
        mId = id;
    }

    private Donation(Parcel in) {
        mId = in.readString();
        mTransactionId = in.readString();
        mTimestamp = in.readLong();
        mAmount = in.readDouble();
        mDistance = in.readDouble();
    }

    public static final Creator<Donation> CREATOR = new Creator<Donation>() {
        @Override
        public Donation createFromParcel(Parcel in) {
            return new Donation(in);
        }

        @Override
        public Donation[] newArray(int size) {
            return new Donation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mTransactionId);
        dest.writeLong(mTimestamp);
        dest.writeDouble(mAmount);
        dest.writeDouble(mDistance);
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getTransactionId() {
        return mTransactionId;
    }

    public void setTransactionId(String transactionId) {
        mTransactionId = transactionId;
    }

    public double getAmount() {
        return mAmount;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }

    public double getDistance() {
        return mDistance;
    }

    public void setDistance(double distance) {
        mDistance = distance;
    }

    public Date getDate() {
        return new Date(mTimestamp);
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(long timestamp) {
        mTimestamp = timestamp;
    }

    public boolean isValid() {
        return mId != null && mAmount > 0 && mDistance > 0 && mTimestamp > 0 &&
                mTransactionId != null;
    }
}
