package com.example.terminator.ring14.utils;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

public abstract class CustomTimer {

    private static final int MSG = 1;

    private final long mInterval;

    private long mStartTime;

    private long mPauseStart;

    private long mPauseTotal;

    private boolean mRunning = false;

    private boolean mPaused = false;

    public CustomTimer(long interval) {
        mInterval = interval;
    }

    public synchronized final void start(long startTime) {
        if (mRunning) {
            stop();
        }
        mRunning = true;
        mPaused = false;
        mStartTime = startTime;
        mPauseTotal = 0;
        mHandler.sendMessage(mHandler.obtainMessage(MSG));
    }

    public synchronized final void start() {
        start(SystemClock.elapsedRealtime());
    }

    public synchronized final void stop() {
        mRunning = false;
        mPaused = false;
        mHandler.removeMessages(MSG);
    }

    public synchronized final void pause() {
        if (!mRunning || mPaused) {
            return;
        }
        mPaused = true;
        mPauseStart = SystemClock.elapsedRealtime();
        mHandler.removeMessages(MSG);
    }

    public synchronized final void resume() {
        if (!mRunning || !mPaused) {
            return;
        }
        mPaused = false;
        mPauseTotal += SystemClock.elapsedRealtime() - mPauseStart;
        mHandler.sendMessage(mHandler.obtainMessage(MSG));
    }

    public synchronized final boolean isRunning() {
        return mRunning;
    }

    public synchronized final boolean isPaused() {
        return mPaused;
    }

    public abstract void onTick(long millisElapsed);

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            synchronized (CustomTimer.this) {
                if (!mRunning || mPaused) {
                    return;
                }

                final long millisElapsed = SystemClock.elapsedRealtime() - mStartTime - mPauseTotal;

                final long lastTickStart = SystemClock.elapsedRealtime();

                onTick(millisElapsed);

                // take into account user's onTick taking time to execute
                long delay = lastTickStart + mInterval - SystemClock.elapsedRealtime();

                // special case: user's onTick took more than interval to
                // complete, skip to next interval
                while (delay < 0) {
                    delay += mInterval;
                }

                sendMessageDelayed(obtainMessage(MSG), delay);
            }
        }
    };
}
