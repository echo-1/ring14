package com.example.terminator.ring14.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.preference.PreferenceManager;
import android.text.format.DateUtils;

import com.example.terminator.ring14.R;
import com.example.terminator.ring14.model.Donation;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class StringGenerator {

    public static String millisToTime(Context context, long millis) {
        Locale locale = getCurrentLocale(context);
        return String.format(locale, context.getString(R.string.time_hhmmss),
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    public static String distanceToText(Context context, double distance) {
        Locale locale = getCurrentLocale(context);
        String metric = context.getString(R.string.metric_system_value);
        String pref = getPreferredUnit(context);
        if (pref.equals(metric)) {
            return String.format(locale, context.getString(R.string.float1_with_unit),
                    distance, context.getString(R.string.unit_km));
        } else {
            return String.format(context.getString(R.string.float1_with_unit),
                    distance * 0.621, context.getString(R.string.unit_mi));
        }
    }

    public static String sessionDistanceToText(Context context, float meters) {
        return distanceToText(context, meters / 1000);
    }

    public static String speedToText(Context context, double metersPerSecond) {
        Locale locale = getCurrentLocale(context);
        String metric = context.getString(R.string.metric_system_value);
        String pref = getPreferredUnit(context);
        if (pref.equals(metric)) {
            return  String.format(locale, context.getString(R.string.float1_with_unit),
                    metersPerSecond * 3.6, context.getString(R.string.unit_kmh));
        } else {
            return String.format(context.getString(R.string.float1_with_unit),
                    metersPerSecond * 2.236, context.getString(R.string.unit_mph));
        }
    }

    public static String moneyToText(Context context, double amount) {
        Locale locale = getCurrentLocale(context);
        NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
        Currency currency = Currency.getInstance("EUR");
        nf.setCurrency(currency);
        return nf.format(amount);
    }

    public static String timestampToShortDateTimeText(Context context, long timestamp) {
        Locale locale = StringGenerator.getCurrentLocale(context);
        DateFormat f = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);
        return f.format(new Date(timestamp));
    }

    public static String getDonationShareMessage(Context context, Donation donation) {
        Locale locale = getCurrentLocale(context);
        int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NO_YEAR;
        String date = DateUtils.formatDateTime(context, donation.getTimestamp(), flags);
        String amount = moneyToText(context, donation.getAmount());
        String distance = distanceToText(context, donation.getDistance());
        String message = context.getString(R.string.intent_share_message);
        return String.format(locale, message, amount, date, distance);
    }

    public static String getNotificationString(Context context, long millis, float distance) {
        Locale locale = getCurrentLocale(context);
        String durationString = StringGenerator.millisToTime(context, millis);
        String distanceString = sessionDistanceToText(context, distance);
        String message = context.getString(R.string.notification_string);
        return String.format(locale, message, durationString, distanceString);
    }

    public static boolean shouldDefaultToMetric(Context context) {
        String countryCode = getCurrentLocale(context).getCountry();
        return !("US".equals(countryCode) || "LR".equals(countryCode) || "MM".equals(countryCode));
    }

    public static String getDefaultUnitSystemValue(Context context) {
        if (shouldDefaultToMetric(context)) {
            return context.getString(R.string.metric_system_value);
        } else {
            return context.getString(R.string.imperial_system_value);
        }
    }

    public static Locale getCurrentLocale(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getResources().getConfiguration().getLocales()
                    .getFirstMatch(context.getResources().getAssets().getLocales());
        } else {
            return context.getResources().getConfiguration().locale;
        }
    }

    public static String getPreferredUnit(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(context.getString(R.string.key_unit_pref),
                getDefaultUnitSystemValue(context));
    }

    public static String getUnitText(Context context) {
        String metric = context.getString(R.string.metric_system_value);
        String pref = getPreferredUnit(context);
        if (pref.equals(metric)) {
            return context.getString(R.string.unit_km);
        } else {
            return context.getString(R.string.unit_mi);
        }
    }
}
