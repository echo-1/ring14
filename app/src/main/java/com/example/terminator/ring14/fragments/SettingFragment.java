package com.example.terminator.ring14.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.webkit.WebView;

import com.example.terminator.ring14.BuildConfig;
import com.example.terminator.ring14.R;
import com.example.terminator.ring14.utils.StringGenerator;

public class SettingFragment extends PreferenceFragmentCompat {

    private static final String KEY_LICENSES = "licenses";
    private static final String KEY_VERSION = "key_pref_version";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);

        Preference licensesPref = findPreference(KEY_LICENSES);
        licensesPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showLicensesDialog();
                return true;
            }
        });
        Preference summary = findPreference(KEY_VERSION);
        summary.setSummary(BuildConfig.VERSION_NAME);

        String unitPrefKey = getString(R.string.key_unit_pref);
        Preference unitPreference = findPreference(unitPrefKey);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String unitPrefValue = sharedPref.getString(unitPrefKey,
                StringGenerator.getDefaultUnitSystemValue(getContext()));
        setUnitPreferenceSummary(unitPreference, unitPrefValue);
        unitPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                setUnitPreferenceSummary(preference, (String) newValue);
                return true;
            }
        });
    }

    private void setUnitPreferenceSummary(Preference preference, String unitValue) {
        if (unitValue.equals(getString(R.string.metric_system_value))) {
            preference.setSummary(R.string.metric_system);
        } else {
            preference.setSummary(R.string.imperial_system);
        }
    }

    private void showLicensesDialog() {
        Context context = getActivity();
        WebView webView = new WebView(context);
        webView.loadUrl("file:///android_asset/licenses.html");

        new AlertDialog.Builder(context)
                .setTitle(getString(R.string.licenses_title))
                .setView(webView)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }
}
