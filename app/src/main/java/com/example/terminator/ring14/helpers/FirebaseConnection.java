package com.example.terminator.ring14.helpers;

import android.os.Handler;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

/**
 * The free plan of Firebase has a limit on the number of simultaneous connections.
 * By default, the connection to the database is automatically closed after a certain
 * amount of time. This class allows to control the window of inactivity to further
 * reduce the number of simultaneous connections.
 * <p>
 * Note: this class manages the connection using {@link FirebaseDatabase#goOffline()}.
 * Accessing the database using {@link FirebaseDatabase} directly all together with this
 * class might have undesidered effects, unless the connection minimization is disabled first.
 */
public class FirebaseConnection {

    public static final long DEFAULT_CONNECTION_TIMEOUT = 10000;

    private static final String TAG = FirebaseConnection.class.getSimpleName();

    private static FirebaseConnection sInstance = null;

    private DatabaseReference mDatabase;

    /**
     * If true, the connection with the database stays open for {@link #DEFAULT_CONNECTION_TIMEOUT}.
     * If false, Firebase will automatically close inactive connections after a predefined
     * amount of time.
     */
    private boolean mMinimizeConnections = false;

    private Handler mHandler = new Handler();

    /* The first connection is enabled automatically */
    private int mActiveConnections = 1;

    private FirebaseConnection() {
        disableDelayed();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    /**
     * @return FirebaseConnection instance
     */
    public static synchronized FirebaseConnection getInstance() {
        if (sInstance == null) {
            sInstance = new FirebaseConnection();
        }
        return sInstance;
    }

    /**
     * Enable Firebase connections minimization.
     *
     * @param minimize Whether or not to enable the minimization
     */
    public synchronized void minimizeConnections(boolean minimize) {
        if (mMinimizeConnections == minimize) {
            return;
        }

        mMinimizeConnections = minimize;

        if (!mMinimizeConnections) {
            mHandler.removeCallbacksAndMessages(null);
            mActiveConnections = 1;
            FirebaseDatabase.getInstance().goOnline();
        } else {
            disableDelayed();
        }
    }

    private synchronized void enable() {
        if (!mMinimizeConnections) {
            return;
        }

        if (mActiveConnections == 0) {
            FirebaseDatabase.getInstance().goOnline();
        }
        mActiveConnections++;
    }

    private synchronized void disable() {
        if (mActiveConnections == 1) {
            FirebaseDatabase.getInstance().goOffline();
        }
        mActiveConnections--;

        if (mActiveConnections < 0) {
            mActiveConnections = 0;
            Log.e(TAG, "Unbalanced connection enable/disable count");
        }
    }

    private void disableDelayed(long timeout) {
        if (!mMinimizeConnections) {
            return;
        }

        if (timeout > 0) {
            mHandler.postDelayed(new Runnable() {
                public void run() {
                    disable();
                }
            }, timeout);
        } else {
            disable();
        }
    }

    private void disableDelayed() {
        disableDelayed(DEFAULT_CONNECTION_TIMEOUT);
    }

    /**
     * Wrapper for {@link DatabaseReference#getKey()} that automatically closes the connection
     * once it's not longer needed.
     *
     * @param databaseReference
     * @return The key created
     */
    public String getKey(DatabaseReference databaseReference) {
        enable();
        return databaseReference.push().getKey();
    }

    /**
     * Same as {@link #getKey(DatabaseReference databaseReference)}, but allows to specify the
     * {@link DatabaseReference} by name.
     *
     * @param child Name of the DatabaseReference
     * @return The key created
     */
    public String getKey(String child) {
        return getKey(mDatabase.child(child));
    }

    /**
     * Same as {@link #getKey(DatabaseReference databaseReference)}, but used the root of the
     * database as {@link DatabaseReference}.
     *
     * @return The key created
     */
    public String getKey() {
        return getKey(mDatabase);
    }

    /**
     * Wrapper for {@link DatabaseReference#setValue(Object, DatabaseReference.CompletionListener)}
     * that automatically closes the connection once it's not longer needed.
     *
     * @param databaseReference  DatabaseReference where to save the given object
     * @param object             Object to save into the database
     * @param completionListener Callback for when the operation has been completed
     * @param timeout            How long the connection should stay open after the object has
     *                           been saved
     */
    public void writeDB(DatabaseReference databaseReference, Object object,
                        final DatabaseReference.CompletionListener completionListener,
                        final long timeout) {
        enable();
        databaseReference.setValue(object,
                new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError,
                                           DatabaseReference databaseReference) {
                        completionListener.onComplete(databaseError, databaseReference);
                        disableDelayed(timeout);
                    }
                });
    }

    /**
     * Same as
     * {@link #writeDB(DatabaseReference, Object, DatabaseReference.CompletionListener, long)},
     * but uses a default timeout value.
     */
    public void writeDB(DatabaseReference databaseReference, Object object,
                        final DatabaseReference.CompletionListener completionListener) {
        writeDB(databaseReference, object, completionListener,
                FirebaseConnection.DEFAULT_CONNECTION_TIMEOUT);
    }

    /**
     * Same as
     * {@link #writeDB(DatabaseReference, Object, DatabaseReference.CompletionListener, long)},
     * but allows to specify the {@link DatabaseReference} by name.
     */
    public void writeDB(String child, Object object,
                        DatabaseReference.CompletionListener completionListener, long timeout) {
        writeDB(mDatabase.child(child), object, completionListener, timeout);
    }

    /**
     * Same as {@link #writeDB(String, Object, DatabaseReference.CompletionListener)},
     * but allows to specify the {@link DatabaseReference} by name.
     */
    public void writeDB(String child, Object object,
                        DatabaseReference.CompletionListener completionListener) {
        writeDB(mDatabase.child(child), object, completionListener,
                FirebaseConnection.DEFAULT_CONNECTION_TIMEOUT);
    }

    public void writeMapDB(Map<String, Object> object,
                           final DatabaseReference.CompletionListener completionListener,
                           final long timeout) {
        enable();
        FirebaseDatabase.getInstance().getReference().updateChildren(object,
                new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError,
                                           DatabaseReference databaseReference) {
                        completionListener.onComplete(databaseError, databaseReference);
                        disableDelayed(timeout);
                    }
                });
    }

    public void writeMapDB(Map<String, Object> object,
                           final DatabaseReference.CompletionListener completionListener) {
        writeMapDB(object, completionListener, FirebaseConnection.DEFAULT_CONNECTION_TIMEOUT);
    }

    /**
     * Wrapper for {@link DatabaseReference#push()} that automatically closes the connection once
     * it's not longer needed.
     *
     * @param databaseReference  DatabaseReference where to save the given object
     * @param object             Object to push into the database
     * @param completionListener Callback for when the operation has been completed
     * @param timeout            How long the connection should stay open after the object has
     *                           been saved
     */
    public String pushDB(DatabaseReference databaseReference, Object object,
                         final DatabaseReference.CompletionListener completionListener,
                         final long timeout) {
        enable();
        final String id = databaseReference.push().getKey();
        databaseReference.child(id).setValue(object,
                new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError,
                                           DatabaseReference databaseReference) {
                        completionListener.onComplete(databaseError, databaseReference);
                        disableDelayed(timeout);
                    }
                });
        return id;
    }

    /**
     * Same as {@link #pushDB(DatabaseReference, Object, DatabaseReference.CompletionListener)},
     * but uses a default timeout value.
     */
    public String pushDB(DatabaseReference databaseReference, Object object,
                         final DatabaseReference.CompletionListener completionListener) {
        return pushDB(databaseReference, object, completionListener,
                FirebaseConnection.DEFAULT_CONNECTION_TIMEOUT);
    }

    /**
     * Same as
     * {@link #pushDB(DatabaseReference, Object, DatabaseReference.CompletionListener, long)},
     * but allows to specify the {@link DatabaseReference} by name.
     */
    public String pushDB(String child, Object object,
                         DatabaseReference.CompletionListener completionListener, long timeout) {
        return pushDB(mDatabase.child(child), object, completionListener, timeout);
    }

    /**
     * Same as {@link #pushDB(String, Object, DatabaseReference.CompletionListener)},
     * but uses a default timeout value.
     */
    public String pushDB(String child, Object object,
                         DatabaseReference.CompletionListener completionListener) {
        return pushDB(mDatabase.child(child), object, completionListener,
                FirebaseConnection.DEFAULT_CONNECTION_TIMEOUT);
    }

    /**
     * Wrapper for {@link DatabaseReference#addValueEventListener(ValueEventListener)}
     * that automatically closes the connection once it's not longer needed.
     *
     * @param databaseReference  DatabaseReference from which the data is retrieved
     * @param valueEventListener Callback for when the data is ready
     * @param timeout            How long the connection should stay open after the object has
     *                           been saved
     */
    public void readDB(DatabaseReference databaseReference,
                       final ValueEventListener valueEventListener, final long timeout) {
        enable();
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                valueEventListener.onDataChange(dataSnapshot);
                disableDelayed(timeout);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                valueEventListener.onCancelled(databaseError);
                disableDelayed(timeout);
            }
        });
    }

    /**
     * Same as {@link #readDB(DatabaseReference, ValueEventListener, long)}, but uses a default
     * timeout value.
     */
    public void readDB(DatabaseReference databaseReference,
                       final ValueEventListener valueEventListener) {
        readDB(databaseReference, valueEventListener,
                FirebaseConnection.DEFAULT_CONNECTION_TIMEOUT);
    }

    /**
     * Same as {@link #readDB(DatabaseReference, ValueEventListener, long)}, but allows to
     * specify the {@link DatabaseReference} by name.
     */
    public void readDB(String child, ValueEventListener valueEventListener, long timeout) {
        readDB(mDatabase.child(child), valueEventListener, timeout);
    }

    /**
     * Same as {@link #readDB(String, ValueEventListener, long)}, but uses a default timeout value.
     */
    public void readDB(String child, ValueEventListener valueEventListener) {
        readDB(mDatabase.child(child), valueEventListener);
    }
}
