package com.example.terminator.ring14.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.example.terminator.ring14.Constants;
import com.example.terminator.ring14.R;
import com.example.terminator.ring14.utils.DonationUtils;
import com.example.terminator.ring14.utils.StringGenerator;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class DonationActivity extends BaseActivity {

    public static final String DONATION_ID_EXTRA = "DONATION_ID";

    private static final String TAG = DonationActivity.class.getSimpleName();

    private static final int BRAINTREE_REQUEST_CODE = 1001;

    private static final String INPUT_MONEY_SENT = "INPUT_MONEY_SENT";

    private static final String IS_SENDING_MONEY = "IS_SENDING_MONEY";

    private String mBraintreeToken = null;

    private boolean mIsServerReady = false;

    private double mInputMoney = 0;

    private double mInputMoneySent = 0;

    private boolean mIsSendingMoney = false;

    private boolean mIsUnitDistance;

    private final AsyncHttpClient mClient = new AsyncHttpClient();

    private final AsyncHttpClient mClientTransaction = new AsyncHttpClient();

    private boolean isActivityDestroyed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mInputMoneySent = savedInstanceState.getInt(INPUT_MONEY_SENT);
            mIsSendingMoney = savedInstanceState.getBoolean(IS_SENDING_MONEY);
        }

        setContentView(R.layout.activity_donation);
        showLoadingAnimation(mIsSendingMoney);

        setupView();

        getServerStatus();
    }

    @Override
    protected void onDestroy() {
        mClient.cancelRequests(this, true);

        /* Keep ongoing transactions alive */
        mClientTransaction.cancelRequests(this, false);
        isActivityDestroyed = true;
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        /*
         * Save the amount of money so that when we get out of the drop-in UI we know
         * how much the user inserted, even if the activity has been killed.
         */
        savedInstanceState.putDouble(INPUT_MONEY_SENT, mInputMoneySent);
        savedInstanceState.putBoolean(IS_SENDING_MONEY, mIsSendingMoney);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void getBraintreeToken() {
        if (Constants.LOCAL_TEST) {
            /* Add a delay to simulate a slow connection */
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBraintreeToken = getString(R.string.braintree_test_token);
                    setupViewOnConnectionChange();
                }
            }, 1500);
            return;
        }

        mClient.get(this, Constants.SERVER_GET_CLIENT_TOKEN_URL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String clientToken) {
                mBraintreeToken = clientToken;
                setupViewOnConnectionChange();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                showServerConnectionErrorMessage(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getBraintreeToken();
                    }
                });
            }
        });
    }

    private void getServerStatus() {
        if (Constants.LOCAL_TEST) {
            mIsServerReady = true;
            getBraintreeToken();
            setupViewOnConnectionChange();
            return;
        }

        mClient.get(this, Constants.SERVER_GET_STATUS_URL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String status) {
                mIsServerReady = Constants.SERVER_READY_RESPONSE_STRING.equals(status);
                setupViewOnConnectionChange();
                if (!mIsServerReady) {
                    showServerConnectionErrorMessage(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getServerStatus();
                        }
                    });
                } else {
                    getBraintreeToken();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                showServerConnectionErrorMessage(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getServerStatus();
                    }
                });
            }
        });
    }

    private void setupViewOnConnectionChange() {
        findViewById(R.id.donation_connecting)
                .setVisibility(isClientFullyConnected() ? View.GONE : View.VISIBLE);
        findViewById(R.id.donate).setEnabled(canDonate());
    }

    private void setupView() {
        // Don't allow null donations
        findViewById(R.id.donate).setEnabled(false);

        Spinner unitSpinner = (Spinner) findViewById(R.id.donation_unit);
        List<String> spinnerValues = new ArrayList<>();
        spinnerValues.add(StringGenerator.getUnitText(this));
        mIsUnitDistance = true; // donation_unit_distance is the first entry
        spinnerValues.add(getString(R.string.unit_euro));
        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerValues);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unitSpinner.setAdapter(spinnerAdapter);
        unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getItemAtPosition(position).toString();
                mIsUnitDistance = StringGenerator.getUnitText(DonationActivity.this)
                        .equals(selected);
                updateEquivalentValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        EditText editText = (EditText) findViewById(R.id.donation_input);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputString = s.toString();
                double inputValue = inputString.isEmpty() ? 0 : Double.parseDouble(inputString);
                updateEquivalentValue(inputValue);

                /* mInputMoney must be updated for this to work */
                findViewById(R.id.donate).setEnabled(canDonate());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BRAINTREE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (Constants.LOCAL_TEST) {
                    /* Add a delay to simulate a slow connection */
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent();
                            intent.putExtra(DONATION_ID_EXTRA, DonationUtils.TEST_DONATION_ID);
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        }
                    }, 1500);
                    return;
                }

                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                double amount = mInputMoneySent;
                String nonce = result.getPaymentMethodNonce().getNonce();
                JSONObject json = new JSONObject();
                StringEntity se;
                try {
                    json.put(Constants.KEY_PAYMENT_NONCE, nonce);
                    json.put(Constants.KEY_PAYMENT_AMOUNT, amount);
                    json.put(Constants.KEY_FIREBASE_UID, getCurrentUser().getUid());
                    se = new StringEntity(json.toString());
                } catch (JSONException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                    showLoadingAnimation(false);
                    showTransactionErrorMessage(getString(R.string
                            .transaction_error_dialog_message));
                    return;
                }

                /*
                 * Don't wait too much here, it's better to tell the user that something's wrong
                 * rather than having the activity killed because nothing was happening.
                 */
                mClientTransaction.setMaxRetriesAndTimeout(2, 5000);
                mClientTransaction.post(this, Constants.SERVER_POST_NONCE_URL, se,
                        "application/json", new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Log.d(TAG, "Transaction went through");
                        if (isActivityDestroyed) {
                            return;
                        }

                        Intent resultIntent = new Intent();
                        String transactionId = null;
                        try {
                            transactionId = new String(responseBody, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            Log.e(TAG, "Could not get transaction id");
                            e.printStackTrace();
                        }
                        resultIntent.putExtra(DONATION_ID_EXTRA, transactionId);
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody,
                                          Throwable error) {
                        if (isActivityDestroyed) {
                            Log.e(TAG, "Transaction error: status=" + statusCode);
                            return;
                        }

                        if (error.getCause() != null) {
                            /*
                             * Connection error: we don't know if the transaction went through
                             * or not. Since the transaction is done on the backend, the user
                             * should see it elsewhere in the app, unless the problem is caused
                             * by a crash.
                             * TODO: inform that there's a chance money were transfered
                             */
                            Log.e(TAG, "Transaction error: " + error.toString());
                            showTransactionErrorMessage(getString(R.string
                                    .transaction_connection_error_dialog_message));
                        } else {
                            /* The server rejected the request. No money transfer happened. */
                            Log.e(TAG, "Transaction error: status=" + statusCode);
                            showTransactionErrorMessage(getString(R.string
                                    .transaction_error_dialog_message));
                        }
                        showLoadingAnimation(false);
                    }
                });
            } else if (resultCode == Activity.RESULT_CANCELED) {
                showLoadingAnimation(false);
            } else {
                showLoadingAnimation(false);
                Log.d(TAG, "Could not start drop-in activity: " +
                        data.getSerializableExtra(DropInActivity.EXTRA_ERROR));
            }
        }
    }

    @Override
    protected void onFirebaseAuthenticated() {
    }

    @Override
    protected void onFirebaseAuthenticationFailure() {
    }

    private boolean isInputValid() {
        return mInputMoney > 0;
    }

    private boolean isTokenAvailable() {
        return mBraintreeToken != null;
    }

    private boolean isClientFullyConnected() {
        return isTokenAvailable() && mIsServerReady;
    }

    private boolean canDonate() {
        return isClientFullyConnected() && isInputValid();
    }

    private void updateEquivalentValue(double inputValue) {
        String equivalentText;
        if (mIsUnitDistance) {
            double equivalentValue = DonationUtils.distanceToEur(this, inputValue);
            mInputMoney = equivalentValue;
            equivalentText = StringGenerator.moneyToText(this, equivalentValue);
        } else {
            double equivalentValue = DonationUtils.eurToDistance(this, inputValue);
            mInputMoney = inputValue;
            equivalentText = StringGenerator.distanceToText(this, equivalentValue);
        }
        TextView inputEquivalent = (TextView) findViewById(R.id.donation_input_equivalent);
        inputEquivalent.setText(equivalentText);
    }

    private void updateEquivalentValue() {
        updateEquivalentValue(getInputValue());
    }

    private double getInputValue() {
        EditText editText = (EditText) findViewById(R.id.donation_input);
        String inputString = editText.getText().toString();
        return inputString.isEmpty() ? 0 : Double.parseDouble(inputString);
    }

    public void doPayment() {
        String amountString = StringGenerator.moneyToText(this, mInputMoney);
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken(mBraintreeToken)
                .amount(amountString);
        mInputMoneySent = mInputMoney;
        startActivityForResult(dropInRequest.getIntent(this), BRAINTREE_REQUEST_CODE);
        showLoadingAnimation(true);
    }

    public void onBuyPressed(View pressed) {
        doPayment();
    }

    private void showLoadingAnimation(boolean show) {
        int height = findViewById(R.id.donation_main_content).getHeight();
        findViewById(R.id.donation_main_content).setVisibility(!show ? View.VISIBLE : View.GONE);
        View loadingView = findViewById(R.id.loading_animation);
        loadingView.setMinimumHeight(height);
        loadingView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showServerConnectionErrorMessage(DialogInterface.OnClickListener retryAction) {
        if (isActivityDestroyed) {
            return;
        }
        new AlertDialog.Builder(this)
                .setTitle(R.string.connection_error_dialog_title)
                .setMessage(R.string.connection_error_dialog_message)
                .setPositiveButton(android.R.string.ok, retryAction)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    }
                })
                .show();
    }

    private void showTransactionErrorMessage(String message) {
        if (isActivityDestroyed) {
            return;
        }
        new AlertDialog.Builder(this)
                .setTitle(R.string.transaction_error_dialog_title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    }
                })
                .show();
    }
}
