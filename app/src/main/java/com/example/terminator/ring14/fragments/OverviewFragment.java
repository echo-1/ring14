package com.example.terminator.ring14.fragments;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.terminator.ring14.Constants;
import com.example.terminator.ring14.R;
import com.example.terminator.ring14.activities.DonationActivity;
import com.example.terminator.ring14.activities.MainActivity;
import com.example.terminator.ring14.helpers.FirebaseConnection;
import com.example.terminator.ring14.interfaces.FirebaseFragment;
import com.example.terminator.ring14.utils.StringGenerator;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import at.grabner.circleprogress.CircleProgressView;

public class OverviewFragment extends Fragment implements FirebaseFragment {

    private static final int ANIMATION_DURATION = 500;

    private static final int RC_DONATION = 1033;

    private static final String KEY_CIRCLE_PROGRESS = "circle_progress";
    private static final String KEY_GOAL_DISTANCE = "goal_distance";
    private static final String KEY_DONATION_DISTANCE = "donated_distance";

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private CircleProgressView mCircleProgressView;

    private TextView mTotalDonatedTV;
    private TextView mTotalGoalTV;

    private float mProgress;
    private int mGoal = 0;
    private int mDistance = 0;

    private boolean mIsCircleSpinning = false;

    public static OverviewFragment newInstance() {
        return new OverviewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);

        final MainActivity activity = (MainActivity) getActivity();
        activity.setTitle(R.string.overview_frag_title);

        TextView titleGoal = (TextView) view.findViewById(R.id.tv_title_goal);
        TextView titleDonated = (TextView) view.findViewById(R.id.tv_title_donated_dist);
        titleGoal.setText(String.format(getString(R.string.overview_goal),
                StringGenerator.getUnitText(activity)));
        titleDonated.setText(String.format(getString(R.string.overview_distance),
                StringGenerator.getUnitText(activity)));
        mCircleProgressView = (CircleProgressView) view.findViewById(R.id.donation_progress);
        mTotalGoalTV = (TextView) view.findViewById(R.id.tv_total_goal);
        mTotalDonatedTV = (TextView) view.findViewById(R.id.tv_total_donated);
        /* Don't animate CircleProgressView on screen orientation change */
        if (savedInstanceState != null) {
            mProgress = savedInstanceState.getFloat(KEY_CIRCLE_PROGRESS);
            mGoal = savedInstanceState.getInt(KEY_GOAL_DISTANCE);
            mDistance = savedInstanceState.getInt(KEY_DONATION_DISTANCE);
            mCircleProgressView.setValue(mProgress);
            mTotalDonatedTV.setText(String.valueOf((int) convertUnits(mDistance)));
            mTotalGoalTV.setText(String.valueOf((int) convertUnits(mGoal)));
        } else {
            // set to zero
            mTotalDonatedTV.setText(String.valueOf(mDistance));
            mTotalGoalTV.setText(String.valueOf(mGoal));

            if (activity.isAuthenticated()) {
                updateData();
            } else {
                mIsCircleSpinning = true;
                mCircleProgressView.spin();
            }
        }

        activity.setFabAction(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, DonationActivity.class);
                startActivityForResult(intent, RC_DONATION);
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.light_blue);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateData();
            }
        });

        return view;
    }

    private void updateData() {
        FirebaseConnection.getInstance().readDB(Constants.FIREBASE_TABLE_TOTAL_ROOT + "/" +
                Constants.DONATION_SESSION, new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Don't do anything if this fragment is no longer attached to its activity
                if (!isAdded()) {
                    return;
                }

                double goal = dataSnapshot.child(Constants.FIREBASE_TABLE_CURRENT_GOAL)
                        .getValue(Double.class);
                mGoal = (int) goal;
                double distance = dataSnapshot.child(Constants.FIREBASE_TABLE_CURRENT_DISTANCE)
                        .getValue(Double.class);
                int mygoal = (int) convertUnits(goal);
                mDistance = (int) distance;
                int mydist = (int) convertUnits(distance);
                mProgress = (float) (distance / goal * 100);

                mSwipeRefreshLayout.setRefreshing(false);

                /* Stop animation and set the right value */
                if (mIsCircleSpinning) {
                    mIsCircleSpinning = false;
                    mCircleProgressView.setValue(0);
                }
                mCircleProgressView.setValueAnimated(mProgress, ANIMATION_DURATION);
                animateCounter(mydist, mTotalDonatedTV);
                animateCounter(mygoal, mTotalGoalTV);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void animateCounter(int count, final TextView textView){
        int value = Integer.parseInt(textView.getText().toString());
        ValueAnimator animator = ValueAnimator.ofInt(value, count);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                textView.setText(String.valueOf(animation.getAnimatedValue()));
            }
        });

        animator.setDuration(ANIMATION_DURATION);
        animator.start();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putFloat(KEY_CIRCLE_PROGRESS, mProgress);
        savedInstanceState.putInt(KEY_GOAL_DISTANCE, mGoal);
        savedInstanceState.putInt(KEY_DONATION_DISTANCE, mDistance);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onAuthenticated() {
        if (getView() == null) {
            /* This method can be called before onCreateView() */
            return;
        }
        updateData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_DONATION) {
            if (resultCode == Activity.RESULT_OK) {
                updateData();

                String donationId = data.getStringExtra(DonationActivity.DONATION_ID_EXTRA);
                ((MainActivity) getActivity()).showSnackbarFor(donationId);
            }
        }
    }

    public double convertUnits(double distance) {
        String metric = getString(R.string.metric_system_value);
        String pref = StringGenerator.getPreferredUnit(getContext());
        if (!pref.equals(metric)) {
            distance *= 0.621;
        }
        return distance;
    }
}
